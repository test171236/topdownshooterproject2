// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSEnemy.h"
#include "AIController.h"
#include "TDSBasicEnemy_WithWeapon.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API ATDSBasicEnemy_WithWeapon : public ATDSEnemy, public ITDSEnemyAIInterface
{
	GENERATED_BODY()
protected:
	UPROPERTY(ReplicatedUsing = OnRep_Weapon)
	class ATDSWeapon* Weapon;
	UFUNCTION()
	void OnRep_Weapon();

	int AmmoCash;
	UPROPERTY(EditDefaultsOnly)
	class UDataTable* WeaponInfoTable = nullptr;

	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* FireMontage;
	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* ReloadMontage;
	UPROPERTY(EditDefaultsOnly)
	FName StartWeaponName;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float AttackRadius = 200;
	UPROPERTY(EditDefaultsOnly)
	FName AttackNotifyName;

	void InitWeapon(ATDSWeapon* newWeapon);
	void Attack_Implementation() override;
	UFUNCTION(NetMulticast, Unreliable)
	void PlayAttackMontage_NetMulticast();
	UFUNCTION(NetMulticast, Unreliable)
	void PlayReloadMontage_NetMulticast();
	
	float GetAttackRadius_Implementation() const override;
	FOnAttackEnd OnAttackEnd;
	FOnAttackEnd* GetOnAttackEndDelegate() override { return &OnAttackEnd; }
	bool isAttacking = false;
	virtual bool CanAttack_Implementation() const;
	virtual bool CanMove_Implementation() const;

	UFUNCTION()
	void AttackStart(FName NotifyName, const struct FBranchingPointNotifyPayload& BranchingPointPayload);
	UFUNCTION()
	void AttackInterupted(UAnimMontage* Montage, bool bInterrupted);

public:
	ATDSBasicEnemy_WithWeapon();

	void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type Reason) override;
};
