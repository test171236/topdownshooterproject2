// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSBasicEnemy.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

ATDSBasicEnemy::ATDSBasicEnemy()
{
    MeleeAttackCollision = CreateDefaultSubobject<UCapsuleComponent>(TEXT("MeleeAttackCollision"));
    MeleeAttackCollision->AttachToComponent(GetMesh(), 
        FAttachmentTransformRules::KeepRelativeTransform,
        TEXT("LeftHandSocket"));
    MeleeAttackCollision->SetCollisionProfileName(TEXT("EnemyMeleeAttack"));
    MeleeAttackCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    MeleeAttackCollision->OnComponentBeginOverlap.AddDynamic(this, &ATDSBasicEnemy::OnMeleeAttackCollisionOverlap);
}

void ATDSBasicEnemy::BeginPlay()
{
    Super::BeginPlay();

    if (HasAuthority())
    {
        UAnimInstance* anim_inst = GetMesh()->GetAnimInstance();
        if (IsValid(anim_inst))
        {
            anim_inst->OnPlayMontageNotifyBegin.AddDynamic(this, &ATDSBasicEnemy::AttackStart);
            anim_inst->OnPlayMontageNotifyEnd.AddDynamic(this, &ATDSBasicEnemy::AttackEnd);
            anim_inst->OnMontageEnded.AddDynamic(this, &ATDSBasicEnemy::AttackInterupted);
        }
    }
}

void ATDSBasicEnemy::Attack_Implementation()
{
    if (!Execute_CanAttack(this))
    {
        OnAttackEnd.Broadcast(EAttackEndResult::Aborted);
        return;
    }

    isAttacking = true;

    PlayAttackMontage_NetMulticast();
}

void ATDSBasicEnemy::PlayAttackMontage_NetMulticast_Implementation()
{
    PlayAnimMontage(MeleeAttackMontage);
}

float ATDSBasicEnemy::GetAttackRadius_Implementation() const
{
    return AttackRadius;
}

bool ATDSBasicEnemy::CanAttack_Implementation() const
{
    return !isAttacking && !isStuned;
}

bool ATDSBasicEnemy::CanMove_Implementation() const
{
    return !isAttacking && !isStuned;
}

void ATDSBasicEnemy::AttackStart(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
    if (HasAuthority())
    {
        if (NotifyName == AttackNotifyName)
        {
            MeleeAttackCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
        }
    }
}

void ATDSBasicEnemy::AttackEnd(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
    if (HasAuthority())
    {
        if (NotifyName == AttackNotifyName)
        {
            MeleeAttackCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
        }
    }
}

void ATDSBasicEnemy::AttackInterupted(UAnimMontage* Montage, bool bInterrupted)
{
    if (HasAuthority())
    {
        if (Montage == MeleeAttackMontage)
        {
            MeleeAttackCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
            isAttacking = false;
            OnAttackEnd.Broadcast(bInterrupted ? EAttackEndResult::Aborted : EAttackEndResult::Succeed);
        }
    }
}

void ATDSBasicEnemy::OnMeleeAttackCollisionOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    if (IsValid(OtherActor))
    {
        OtherActor->TakeDamage(MeleeDamage, FDamageEvent(), GetController(), this);
    }
}
