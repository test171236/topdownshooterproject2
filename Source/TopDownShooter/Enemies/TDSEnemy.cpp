// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSEnemy.h"

#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"
#include "Components/DecalComponent.h"
#include "Components/AudioComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Engine/DecalActor.h"
#include "Engine/ActorChannel.h"
#include "AIController.h"
#include "Net/UnrealNetwork.h"

#include "../Game/TDSGameState.h"
#include "../Character/TDSHealthSystem.h"
#include "../Effects/TDSStatusEffect.h"
#include "../Items/TDSDropComponent.h"

ATDSEnemy::ATDSEnemy()
{
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;

	GetMesh()->SetCollisionProfileName(TEXT("NoCollision"));
	GetCapsuleComponent()->SetCollisionProfileName(TEXT("Enemy"));

	HealthSystem = CreateDefaultSubobject<UTDSHealthSystem>("HealthSystem");
	HealthSystem->OnDeath.AddDynamic(this, &ATDSEnemy::Death);
	//HealthSystem->OnHealthChange.AddDynamic(this, &ATopDownShooterCharacter::OnHeatlhChange);
	HealthSystem->SetIsReplicated(true);

	DropComponent = CreateDefaultSubobject<UTDSDropComponent>("DropComponent");
	DropComponent->SetIsReplicated(true);

	Voice = CreateDefaultSubobject<UAudioComponent>("Voice");
	Voice->SetupAttachment(RootComponent);
}

void ATDSEnemy::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATDSEnemy, StatusEffectList);
}

void ATDSEnemy::BeginPlay()
{
	Super::BeginPlay();

	if (GetLocalRole() == ENetRole::ROLE_Authority)
	{
		ATDSGameState* gs = Cast<ATDSGameState>(UGameplayStatics::GetGameState(GetWorld()));
		if (IsValid(gs))
		{
			gs->AddEnemy(this);
		}
	}
}

void ATDSEnemy::EndPlay(EEndPlayReason::Type Reason)
{
	if (GetLocalRole() == ENetRole::ROLE_Authority)
	{
		ATDSGameState* gs = Cast<ATDSGameState>(UGameplayStatics::GetGameState(GetWorld()));
		if (IsValid(gs))
		{
			gs->RemoveEnemy(this);
		}
	}

	Super::EndPlay(Reason);
}

void ATDSEnemy::Death()
{
	GetCharacterMovement()->StopMovementImmediately();

	if (HasAuthority())
	{
		OnEnemyDeath.Broadcast(this);

		AController* cont = GetController();

		cont->UnPossess();
		if (IsValid(cont))
		{
			cont->Destroy();
		}

		while (StatusEffectList.Num() > 0)
		{
			StatusEffectList[0]->DestroyEffect();
		}

		DropComponent->DropItem();
	}

	GetCapsuleComponent()->SetCollisionProfileName(TEXT("CharacterMesh"));

	if (DeathMontageList.Num() > 0)
	{
		int death_ind = FMath::RandRange(0, DeathMontageList.Num() - 1);
		float dur = PlayAnimMontage(DeathMontageList[death_ind]);
		FTimerHandle th;
		GetWorld()->GetTimerManager().SetTimer(th,
			this,
			&ATDSEnemy::DeathAnimEnd,
			dur);
	}
}

void ATDSEnemy::DeathAnimEnd()
{
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh()->SetCollisionProfileName(TEXT("Ragdoll"));
	GetMesh()->SetAllBodiesSimulatePhysics(true);
	SetLifeSpan(10);
}

bool ATDSEnemy::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (auto effect : StatusEffectList)
	{
		if (effect)
		{
			WroteSomething |= Channel->ReplicateSubobject(effect, *Bunch, *RepFlags);
		}
	}
	return WroteSomething;
}

float ATDSEnemy::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float res = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	res = HealthSystem->ReduceHP(res);
	if (res > 0 && HealthSystem->GetCurShield() <= 0)
	{
		TakeDamageCosmetics_NetMulticast();
	}

	return res;
}

void ATDSEnemy::TakeDamageCosmetics_NetMulticast_Implementation()
{
	if (FMath::FRand() < 0.3)
	{
		FHitResult hr;
		FVector dir = FVector(0, 0, -1).RotateAngleAxis(FMath::FRandRange(0, 45), FVector(1, 0, 0)).RotateAngleAxis(FMath::FRandRange(0, 360), FVector(0, 0, 1));

		UKismetSystemLibrary::LineTraceSingle(GetWorld(),
			GetActorLocation(),
			GetActorLocation() + dir * 1000.f,
			UEngineTypes::ConvertToTraceType(ECC_GameTraceChannel1),
			false,
			TArray<AActor*>({ this }),
			EDrawDebugTrace::None,
			hr,
			true);

		if (hr.bBlockingHit)
		{
			FRotator rot = hr.ImpactNormal.RotateAngleAxis(90, FVector(0, 1, 0)).Rotation();
			rot.Yaw = FMath::FRandRange(0, 360);

			ADecalActor* da = GetWorld()->SpawnActor<ADecalActor>(ADecalActor::StaticClass(),
				FTransform(rot, hr.ImpactPoint));

			da->GetDecal()->DecalSize = FVector(1, 100, 100);
			da->GetDecal()->SetMaterial(0, BloodDropMaterial);
		}
	}

	if (!Voice->IsPlaying())
	{
		Voice->Sound = TakeDamageSound;
		Voice->Play();
	}
}

bool ATDSEnemy::CanBeAffected() const 
{
	return !HealthSystem->GetIsDead();
}

void ATDSEnemy::BeginStun()
{
	isStuned = true;

	PlayAnimMontage(StunAnimMontage);
	AAIController* contr = Cast<AAIController>(GetController());
	if (contr)
	{
		contr->StopMovement();
	}
}

void ATDSEnemy::EndStun()
{
	isStuned = false;

	StopAnimMontage(StunAnimMontage);
}
