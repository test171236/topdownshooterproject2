// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSBasicEnemy_WithWeapon.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Net/UnrealNetwork.h"

#include "../Character/Weapon/TDSWeapon.h"

ATDSBasicEnemy_WithWeapon::ATDSBasicEnemy_WithWeapon()
{
}

void ATDSBasicEnemy_WithWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATDSBasicEnemy_WithWeapon, Weapon);
}

void ATDSBasicEnemy_WithWeapon::OnRep_Weapon(void)
{
    if (IsValid(Weapon))
    {
        FAttachmentTransformRules rule(EAttachmentRule::KeepRelative, false);
        Weapon->AttachToComponent(this->GetMesh(), rule, TEXT("WeaponSocket"));
        Weapon->SetActorRelativeTransform(FTransform());
        Weapon->Mesh->SetVisibility(true);
        Weapon->SetOwner(this);
    }
}

void ATDSBasicEnemy_WithWeapon::BeginPlay()
{
    Super::BeginPlay();

    if (HasAuthority())
    {
        if (!WeaponInfoTable)
        {
            return;
        }

        FWeaponInfo* info = WeaponInfoTable->FindRow<FWeaponInfo>(StartWeaponName, "");

        if (info)
        {
            FActorSpawnParameters asp;
            asp.Owner = this;
            asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

            ATDSWeapon* newWeapon = GetWorld()->SpawnActor<ATDSWeapon>(info->WeaponClass, asp);
            if (newWeapon)
            {
                newWeapon->InitWeapon(*info);
                InitWeapon(newWeapon);
            }
        }

        UAnimInstance* anim_inst = GetMesh()->GetAnimInstance();
        if (IsValid(anim_inst))
        {
            anim_inst->OnPlayMontageNotifyBegin.AddDynamic(this, &ATDSBasicEnemy_WithWeapon::AttackStart);
            anim_inst->OnMontageEnded.AddDynamic(this, &ATDSBasicEnemy_WithWeapon::AttackInterupted);
        }
    }
}

void ATDSBasicEnemy_WithWeapon::EndPlay(EEndPlayReason::Type Reason)
{
    Super::EndPlay(Reason);

    if (HasAuthority())
    {
        if (IsValid(Weapon))
        {
            Weapon->Destroy();
        }
    }
}

void ATDSBasicEnemy_WithWeapon::InitWeapon(ATDSWeapon* newWeapon)
{
    if (IsValid(Weapon))
    {
        Weapon->Destroy();
    }

    Weapon = newWeapon;

    if (IsValid(Weapon))
    {
        FAttachmentTransformRules rule(EAttachmentRule::KeepRelative, false);
        Weapon->AttachToComponent(this->GetMesh(), rule, TEXT("WeaponSocket"));
        Weapon->SetActorRelativeTransform(FTransform());
        Weapon->Mesh->SetVisibility(true);
        Weapon->SetOwner(this);
    }
}

void ATDSBasicEnemy_WithWeapon::Attack_Implementation()
{
    if (HasAuthority())
    {
        if (!ITDSEnemyAIInterface::Execute_CanAttack(this))
        {
            OnAttackEnd.Broadcast(EAttackEndResult::Aborted);
            return;
        }

        isAttacking = true;
        if (Weapon->GetCurrentRounds() <= 0)
        {
            AmmoCash = Weapon->GetMaxRounds();
            Weapon->AuthReloadStart(&AmmoCash);
            PlayReloadMontage_NetMulticast();
        }
        else {
            PlayAttackMontage_NetMulticast();
        }
    }
}

void ATDSBasicEnemy_WithWeapon::PlayAttackMontage_NetMulticast_Implementation()
{
    PlayAnimMontage(FireMontage);
}

void ATDSBasicEnemy_WithWeapon::PlayReloadMontage_NetMulticast_Implementation()
{
    PlayAnimMontage(ReloadMontage);
}

float ATDSBasicEnemy_WithWeapon::GetAttackRadius_Implementation() const
{
    return AttackRadius;
}

bool ATDSBasicEnemy_WithWeapon::CanAttack_Implementation() const
{
    return !isAttacking && !isStuned;
}

bool ATDSBasicEnemy_WithWeapon::CanMove_Implementation() const
{
    return !isAttacking && !isStuned;
}

void ATDSBasicEnemy_WithWeapon::AttackStart(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
    if (HasAuthority())
    {
        if (NotifyName == AttackNotifyName)
        {
            Weapon->AuthFire();
        }
    }
}

void ATDSBasicEnemy_WithWeapon::AttackInterupted(UAnimMontage* Montage, bool bInterrupted)
{
    if (HasAuthority())
    {
        if (Montage == FireMontage || Montage == ReloadMontage)
        {
            isAttacking = false;
            OnAttackEnd.Broadcast(bInterrupted ? EAttackEndResult::Aborted : EAttackEndResult::Succeed);
        }
    }
}
