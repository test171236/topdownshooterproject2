// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSEnemy.h"
#include "TDSEnemyAIInterface.h"
#include "TDSBoss.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API ATDSBoss : public ATDSEnemy, public ITDSBossAIInterface
{
	GENERATED_BODY()
	
private:
	UPROPERTY()
	TArray<class ATDSBossMinion *> MinionList;

	void ThrowActor();

	UFUNCTION()
	void OnMinionDeath(class ATDSEnemy* Enemy);
protected:
	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* MeleeAttackMontage;
	UPROPERTY(EditDefaultsOnly)
	class UCapsuleComponent* MeleeAttackCollision;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float AttackRadius;
	UPROPERTY(EditDefaultsOnly)
	float MeleeDamage = 20;
	UPROPERTY(EditDefaultsOnly)
	FName AttackNotifyName;

	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* ThrowAttackMontage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float ThrowAttackRadius;
	UPROPERTY(EditDefaultsOnly)
	FName ThrowAttackSpawnNotifyName;
	UPROPERTY(EditDefaultsOnly)
	FName ThrowAttackThrowNotifyName;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ATDSBossMinion> ThrowingActor;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UTDSBossHealthWidget> HealthWidgetClass;

	UPROPERTY()
	class ATDSBossMinion* CurrentThrowingActor = nullptr;
	UPROPERTY()
	class UTDSBossHealthWidget* HealthWidget;

	UPROPERTY(EditDefaultsOnly)
	float SpecialAttackDelay = 5.f;
	UPROPERTY()
	FTimerHandle SpecialAttackTimer;
	bool IsSpecialAttackRady = true;

	void Attack_Implementation() override;
	float GetAttackRadius_Implementation() const override;
	FOnAttackEnd OnAttackEnd;
	FOnAttackEnd* GetOnAttackEndDelegate() override { return &OnAttackEnd; }
	bool isAttacking = false;
	virtual bool CanAttack_Implementation() const;
	virtual bool CanMove_Implementation() const;
	virtual void SpecialAttack_Implementation();
	virtual float GetSpecialAttackRadius_Implementation() const;
	virtual bool CanUseSpecialAttack_Implementation() const;
	
	UFUNCTION(NetMulticast, Unreliable)
	void PlayAttackMontage_NetMulticast();
	UFUNCTION(NetMulticast, Unreliable)
	void PlayThrowMontage_NetMulticast();

	void Death() override;

	UFUNCTION()
	void AttackStart(FName NotifyName, const struct FBranchingPointNotifyPayload& BranchingPointPayload);
	UFUNCTION()
	void AttackEnd(FName NotifyName, const struct FBranchingPointNotifyPayload& BranchingPointPayload);
	UFUNCTION()
	void AttackInterupted(UAnimMontage* Montage, bool bInterrupted);

	UFUNCTION()
	void OnMeleeAttackCollisionOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:
	ATDSBoss();

	void BeginPlay() override;
};
