// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSEnemy.h"
#include "AIController.h"
#include "TDSBasicEnemy.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API ATDSBasicEnemy : public ATDSEnemy, public ITDSEnemyAIInterface
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* MeleeAttackMontage;
	UPROPERTY(EditDefaultsOnly)
	class UCapsuleComponent* MeleeAttackCollision;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float AttackRadius;
	UPROPERTY(EditDefaultsOnly)
	float MeleeDamage = 10;
	UPROPERTY(EditDefaultsOnly)
	FName AttackNotifyName;

	void Attack_Implementation() override;
	UFUNCTION(NetMulticast, Unreliable)
	void PlayAttackMontage_NetMulticast();
	float GetAttackRadius_Implementation() const override;
	FOnAttackEnd OnAttackEnd;
	FOnAttackEnd* GetOnAttackEndDelegate() override { return &OnAttackEnd; }
	bool isAttacking = false;
	virtual bool CanAttack_Implementation() const;
	virtual bool CanMove_Implementation() const;

	UFUNCTION()
	void AttackStart(FName NotifyName, const struct FBranchingPointNotifyPayload& BranchingPointPayload);
	UFUNCTION()
	void AttackEnd(FName NotifyName, const struct FBranchingPointNotifyPayload& BranchingPointPayload);
	UFUNCTION()
	void AttackInterupted(UAnimMontage* Montage, bool bInterrupted);

	UFUNCTION()
	void OnMeleeAttackCollisionOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
public:
	ATDSBasicEnemy();

	void BeginPlay() override;
};
