// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSBossMinion.h"
#include "Components/CapsuleComponent.h"
#include "Components/MeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

ATDSBossMinion::ATDSBossMinion()
{
    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.bStartWithTickEnabled = true;
    PrimaryActorTick.TickInterval = 0.1f;

    GetMesh()->DestroyComponent();
}

void ATDSBossMinion::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    TArray<AActor*> arr;
    GetOverlappingActors(arr);

    for (AActor* act : arr)
    {
        act->TakeDamage(DamagePerSec * DeltaTime, FDamageEvent(), GetController(), this);
    }
}

void ATDSBossMinion::Death()
{
    Super::Death();

    Destroy();
}

void ATDSBossMinion::BeginPlay()
{
    ACharacter::BeginPlay();
}

void ATDSBossMinion::EndPlay(EEndPlayReason::Type Reason)
{
    ACharacter::EndPlay(Reason);
}
