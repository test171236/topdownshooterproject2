// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSBoss.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Blueprint/UserWidget.h"
#include "TDSBossMinion.h"
#include "../Widgets/TDSBossHealthWidget.h"

ATDSBoss::ATDSBoss()
{
    MeleeAttackCollision = CreateDefaultSubobject<UCapsuleComponent>(TEXT("MeleeAttackCollision"));
    MeleeAttackCollision->AttachToComponent(GetMesh(),
        FAttachmentTransformRules::KeepRelativeTransform,
        TEXT("LeftHandSocket"));
    MeleeAttackCollision->SetCollisionProfileName(TEXT("EnemyMeleeAttack"));
    MeleeAttackCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    MeleeAttackCollision->OnComponentBeginOverlap.AddDynamic(this, &ATDSBoss::OnMeleeAttackCollisionOverlap);
}

void ATDSBoss::BeginPlay()
{
    Super::BeginPlay();

    if (HasAuthority())
    {
        UAnimInstance* anim_inst = GetMesh()->GetAnimInstance();
        if (IsValid(anim_inst))
        {
            anim_inst->OnPlayMontageNotifyBegin.AddDynamic(this, &ATDSBoss::AttackStart);
            anim_inst->OnPlayMontageNotifyEnd.AddDynamic(this, &ATDSBoss::AttackEnd);
            anim_inst->OnMontageEnded.AddDynamic(this, &ATDSBoss::AttackInterupted);
        }
    }

    HealthWidget = CreateWidget<UTDSBossHealthWidget>(GetWorld(), HealthWidgetClass);
    if (HealthWidget)
    {
        HealthWidget->Init(HealthSystem);
        HealthWidget->AddToViewport();
    }
}

void ATDSBoss::ThrowActor()
{
    if (IsValid(CurrentThrowingActor))
    {
        CurrentThrowingActor->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
        CurrentThrowingActor->SetActorRotation(FRotator::ZeroRotator);
        UCharacterMovementComponent* comp = CurrentThrowingActor->GetCharacterMovement();
        comp->Activate(true);
        comp->Launch(GetActorRotation().RotateVector(FVector(1000, 0, 100)));
        CurrentThrowingActor->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
        //CurrentThrowingActor->GetCapsuleComponent()->SetCollisionProfileName(TEXT("BossMinion"));
        CurrentThrowingActor = nullptr;
    }
}

void ATDSBoss::OnMinionDeath(ATDSEnemy* Enemy)
{
    MinionList.Remove(Cast<ATDSBossMinion>(Enemy));
}

void ATDSBoss::Attack_Implementation()
{
    if (HasAuthority())
    {
        if (!Execute_CanAttack(this))
        {
            OnAttackEnd.Broadcast(EAttackEndResult::Aborted);
            return;
        }

        isAttacking = true;
    }
    PlayAttackMontage_NetMulticast();
}

float ATDSBoss::GetAttackRadius_Implementation() const
{
    return AttackRadius;
}

bool ATDSBoss::CanAttack_Implementation() const
{
    return !isAttacking && !isStuned;
}

bool ATDSBoss::CanMove_Implementation() const
{
    return !isAttacking && !isStuned;
}

void ATDSBoss::SpecialAttack_Implementation()
{
    if (HasAuthority())
    {
        if (!Execute_CanUseSpecialAttack(this))
        {
            OnAttackEnd.Broadcast(EAttackEndResult::Aborted);
            return;
        }

        IsSpecialAttackRady = false;
        GetWorld()->GetTimerManager().SetTimer(SpecialAttackTimer, [this]() {this->IsSpecialAttackRady = true; }, SpecialAttackDelay, false);

        isAttacking = true;
    }

    PlayThrowMontage_NetMulticast();
}

float ATDSBoss::GetSpecialAttackRadius_Implementation() const
{
    return ThrowAttackRadius;
}

bool ATDSBoss::CanUseSpecialAttack_Implementation() const
{
    return !isAttacking && !isStuned && IsSpecialAttackRady;
}

void ATDSBoss::PlayAttackMontage_NetMulticast_Implementation()
{
    PlayAnimMontage(MeleeAttackMontage);
}

void ATDSBoss::PlayThrowMontage_NetMulticast_Implementation()
{
    PlayAnimMontage(ThrowAttackMontage);
}

void ATDSBoss::Death()
{
    Super::Death();

    if (HealthWidget)
    {
        HealthWidget->RemoveFromViewport();
    }

    if (HasAuthority())
    {
        while (MinionList.Num() > 0)
        {
            MinionList[0]->Death();
        }
    }
}

void ATDSBoss::AttackStart(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
    if (NotifyName == AttackNotifyName)
    {
        MeleeAttackCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    }
    else if (NotifyName == ThrowAttackSpawnNotifyName && !IsValid(CurrentThrowingActor))
    {
       //GetWorld()->SpawnActor<AActor>(ThrowingActor, FTransform(FVector(0,0,100)));
        CurrentThrowingActor = GetWorld()->SpawnActorDeferred<ATDSBossMinion>(ThrowingActor,
            FTransform(), nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
        if (CurrentThrowingActor)
        {
            CurrentThrowingActor->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
            CurrentThrowingActor->AttachToComponent(GetMesh(),
                FAttachmentTransformRules(EAttachmentRule::KeepRelative, EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, false),
                TEXT("RightHandSocket"));
            CurrentThrowingActor->SetActorRelativeLocation(FVector(0));
            CurrentThrowingActor->FinishSpawning(FTransform(), true);
            
            MinionList.AddUnique(CurrentThrowingActor);
            CurrentThrowingActor->OnEnemyDeath.AddDynamic(this, &ATDSBoss::OnMinionDeath);
        } 
    }
    else if (NotifyName == ThrowAttackThrowNotifyName && IsValid(CurrentThrowingActor))
    {
        ThrowActor();
    }
}

void ATDSBoss::AttackEnd(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
    if (NotifyName == AttackNotifyName)
    {
        MeleeAttackCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    }
}

void ATDSBoss::AttackInterupted(UAnimMontage* Montage, bool bInterrupted)
{
    if (Montage == MeleeAttackMontage)
    {
        MeleeAttackCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
        isAttacking = false;
        OnAttackEnd.Broadcast(bInterrupted ? EAttackEndResult::Aborted : EAttackEndResult::Succeed);
    }
    else if (Montage == ThrowAttackMontage)
    {
        ThrowActor();
        isAttacking = false;
        OnAttackEnd.Broadcast(bInterrupted ? EAttackEndResult::Aborted : EAttackEndResult::Succeed);
    }
}

void ATDSBoss::OnMeleeAttackCollisionOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    if (IsValid(OtherActor))
    {
        OtherActor->TakeDamage(MeleeDamage, FDamageEvent(), GetController(), this);
    }
}
