// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../Effects/TDSAffectedActor.h"
#include "TDSEnemyAIInterface.h"
#include "TDSEnemy.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSEnemy : public ACharacter, public ITDSAffectedActor
{
	GENERATED_BODY()

	UPROPERTY(ReplicatedUsing = OnRep_EffectsList)
	TArray<class UTDSStatusEffect*> StatusEffectList;
	UFUNCTION()
	void OnRep_EffectsList(const TArray<class UTDSStatusEffect*>& oldEffectsList) override { ITDSAffectedActor::OnRep_EffectsList(oldEffectsList); }
	
	UPROPERTY(EditAnywhere)
	TArray<class UAnimMontage*> DeathMontageList;
	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* StunAnimMontage;
	UPROPERTY(EditDefaultsOnly)
	class UMaterialInterface* BloodDropMaterial;
	UPROPERTY(EditAnywhere)
	class UTDSDropComponent* DropComponent;
	UPROPERTY(EditAnywhere)
	class USoundBase* TakeDamageSound;

public:
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEnemyDeath, ATDSEnemy*, Enemy);
	UPROPERTY(BlueprintAssignable)
	FOnEnemyDeath	OnEnemyDeath;

	UPROPERTY(EditAnywhere)
	class UTDSHealthSystem* HealthSystem;

	UPROPERTY(EditAnywhere)
	class UAudioComponent* Voice;

	ATDSEnemy();

protected:
	UPROPERTY(BlueprintReadOnly, Replicated)
	bool isStuned = false;

	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type Reason) override;
	
	UFUNCTION()
	virtual void Death();
	UFUNCTION()
	virtual void DeathAnimEnd();

	bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

public:
	TArray<class UTDSStatusEffect*>& GetEffectsList() override { return StatusEffectList; }
	bool CanBeAffected() const override;
	void BeginStun() override;
	void EndStun() override;

	UFUNCTION(BlueprintCallable)
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser) override;
	UFUNCTION(NetMulticast, Unreliable)
	void TakeDamageCosmetics_NetMulticast();
	
	UFUNCTION(BlueprintCallable)
	bool GetIsStuned() const { return isStuned; }
};
