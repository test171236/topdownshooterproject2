// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSEnemy.h"
#include "TDSBossMinion.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API ATDSBossMinion : public ATDSEnemy
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere)
	float DamagePerSec = 1.f;

public:
	ATDSBossMinion();

	void Tick(float DeltaTime) override;
	void Death() override;
	void BeginPlay() override;
	void EndPlay(EEndPlayReason::Type Reason) override;
};
