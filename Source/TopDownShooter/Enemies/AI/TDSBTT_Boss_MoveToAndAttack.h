// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BehaviorTree/BehaviorTreeTypes.h"
#include "../TDSEnemyAIInterface.h"
#include "TDSBTT_Boss_MoveToAndAttack.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSBTT_Boss_MoveToAndAttack : public UBTTaskNode
{
	GENERATED_BODY()

	UFUNCTION()
	void OnMoveComplited(FAIRequestID RequestID, EPathFollowingResult::Type Result);
	UFUNCTION()
	void OnAttackComplited(EAttackEndResult Result);

	void Attack();

	bool isMoving;
	UBehaviorTreeComponent* OwnerCompPtr;
	AAIController* Controller;

public:
	UPROPERTY(Category = Node, EditAnywhere)
	FBlackboardKeySelector TargetActorKey;

	UTDSBTT_Boss_MoveToAndAttack(const FObjectInitializer& ObjectInitializer);
	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	EBTNodeResult::Type AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
