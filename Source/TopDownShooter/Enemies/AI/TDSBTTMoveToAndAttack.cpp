// Fill out your copyright notice in the Description page of Project Settings.

#include "TDSBTTMoveToAndAttack.h"

#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "BehaviorTree/BlackboardComponent.h"

void UTDSBTTMoveToAndAttack::OnMoveComplited(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
    Controller->ReceiveMoveCompleted.RemoveDynamic(this, &UTDSBTTMoveToAndAttack::OnMoveComplited);

    isMoving = false;

    switch (Result)
    {
    case EPathFollowingResult::Success:
        Attack();
        break;
    case EPathFollowingResult::Aborted:
        FinishLatentTask(*OwnerCompPtr, EBTNodeResult::Aborted);
        break;
    case EPathFollowingResult::Blocked:
    case EPathFollowingResult::OffPath:
    case EPathFollowingResult::Skipped_DEPRECATED:
    case EPathFollowingResult::Invalid:
    default:
        FinishLatentTask(*OwnerCompPtr, EBTNodeResult::Failed);
        break;
    }
}

void UTDSBTTMoveToAndAttack::OnAttackComplited(EAttackEndResult Result)
{
    APawn* pawn = Controller->GetPawn();
    ITDSEnemyAIInterface* interface = Cast<ITDSEnemyAIInterface>(pawn);
    if (interface)
    {
        FOnAttackEnd* delegate = interface->GetOnAttackEndDelegate();
        if (delegate)
        {
            delegate->RemoveDynamic(this, &UTDSBTTMoveToAndAttack::OnAttackComplited);
        }
    }
    FinishLatentTask(*OwnerCompPtr, EBTNodeResult::Succeeded);
}

void UTDSBTTMoveToAndAttack::Attack()
{
    APawn* pawn = Controller->GetPawn();

    ITDSEnemyAIInterface* interface = Cast<ITDSEnemyAIInterface>(pawn);
    if (interface)
    {
        FOnAttackEnd* delegate = interface->GetOnAttackEndDelegate();
        if (delegate)
        {
            delegate->AddDynamic(this, &UTDSBTTMoveToAndAttack::OnAttackComplited);
        }
        else
        {
            FinishLatentTask(*OwnerCompPtr, EBTNodeResult::Succeeded);
        }
    }

    ITDSEnemyAIInterface::Execute_Attack(pawn);
}

UTDSBTTMoveToAndAttack::UTDSBTTMoveToAndAttack(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
    NodeName = "MoveToAndAttack";
    bCreateNodeInstance = true;
}

EBTNodeResult::Type UTDSBTTMoveToAndAttack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    Controller = Cast<AAIController>(OwnerComp.GetOwner());
    APawn* pawn = Controller->GetPawn();
    const UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
    AActor* target = Cast<AActor>(BlackboardComp->GetValueAsObject(TargetActorKey.SelectedKeyName));
    isMoving = false;
    OwnerCompPtr = &OwnerComp;
    if (Controller && target && pawn)
    {
        EPathFollowingRequestResult::Type res = Controller->MoveToActor(target, ITDSEnemyAIInterface::Execute_GetAttackRadius(pawn));
        switch (res)
        {
        case EPathFollowingRequestResult::Failed:
            return EBTNodeResult::Failed;
        case EPathFollowingRequestResult::AlreadyAtGoal:
            Attack();
            break;
        case EPathFollowingRequestResult::RequestSuccessful:
            isMoving = true;
            Controller->ReceiveMoveCompleted.AddDynamic(this, &UTDSBTTMoveToAndAttack::OnMoveComplited);
            break;
        }
    }
    else
    {
        return EBTNodeResult::Failed;
    }

    return EBTNodeResult::InProgress;
}

EBTNodeResult::Type UTDSBTTMoveToAndAttack::AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    if (isMoving)
    {
        if (Controller)
        {
            Controller->StopMovement();
        }
    }

	return EBTNodeResult::Aborted;
}
