// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSBTT_Boss_UseSpecialAttack.h"

#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "BehaviorTree/BlackboardComponent.h"

void UTDSBTT_Boss_UseSpecialAttack::OnMoveComplited(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
    Controller->ReceiveMoveCompleted.RemoveDynamic(this, &UTDSBTT_Boss_UseSpecialAttack::OnMoveComplited);

    isMoving = false;

    switch (Result)
    {
    case EPathFollowingResult::Success:
        Attack();
        break;
    case EPathFollowingResult::Aborted:
        FinishLatentTask(*OwnerCompPtr, EBTNodeResult::Aborted);
        break;
    case EPathFollowingResult::Blocked:
    case EPathFollowingResult::OffPath:
    case EPathFollowingResult::Skipped_DEPRECATED:
    case EPathFollowingResult::Invalid:
    default:
        FinishLatentTask(*OwnerCompPtr, EBTNodeResult::Failed);
        break;
    }
}

void UTDSBTT_Boss_UseSpecialAttack::OnAttackComplited(EAttackEndResult Result)
{
    APawn* pawn = Controller->GetPawn();
    ITDSBossAIInterface* interface = Cast<ITDSBossAIInterface>(pawn);
    if (interface)
    {
        FOnAttackEnd* delegate = interface->GetOnAttackEndDelegate();
        if (delegate)
        {
            delegate->RemoveDynamic(this, &UTDSBTT_Boss_UseSpecialAttack::OnAttackComplited);
        }
    }
    FinishLatentTask(*OwnerCompPtr, EBTNodeResult::Succeeded);
}

void UTDSBTT_Boss_UseSpecialAttack::Attack()
{
    APawn* pawn = Controller->GetPawn();

    ITDSBossAIInterface* interface = Cast<ITDSBossAIInterface>(pawn);
    if (interface)
    {
        FOnAttackEnd* delegate = interface->GetOnAttackEndDelegate();
        if (delegate)
        {
            delegate->AddDynamic(this, &UTDSBTT_Boss_UseSpecialAttack::OnAttackComplited);
        }
        else
        {
            FinishLatentTask(*OwnerCompPtr, EBTNodeResult::Succeeded);
        }
    }

    ITDSBossAIInterface::Execute_SpecialAttack(pawn);
}

UTDSBTT_Boss_UseSpecialAttack::UTDSBTT_Boss_UseSpecialAttack(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
    NodeName = "SpecialAttack";
    bCreateNodeInstance = true;
}

EBTNodeResult::Type UTDSBTT_Boss_UseSpecialAttack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    Controller = Cast<AAIController>(OwnerComp.GetOwner());
    APawn* pawn = Controller->GetPawn();
    const UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
    AActor* target = Cast<AActor>(BlackboardComp->GetValueAsObject(TargetActorKey.SelectedKeyName));
    isMoving = false;
    OwnerCompPtr = &OwnerComp;
    if (Controller && target && pawn)
    {
        EPathFollowingRequestResult::Type res = Controller->MoveToActor(target, ITDSBossAIInterface::Execute_GetSpecialAttackRadius(pawn));
        switch (res)
        {
        case EPathFollowingRequestResult::Failed:
            return EBTNodeResult::Failed;
        case EPathFollowingRequestResult::AlreadyAtGoal:
            Attack();
            break;
        case EPathFollowingRequestResult::RequestSuccessful:
            isMoving = true;
            Controller->ReceiveMoveCompleted.AddDynamic(this, &UTDSBTT_Boss_UseSpecialAttack::OnMoveComplited);
            break;
        }
    }
    else
    {
        return EBTNodeResult::Failed;
    }

    return EBTNodeResult::InProgress;
}

EBTNodeResult::Type UTDSBTT_Boss_UseSpecialAttack::AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    if (isMoving)
    {
        if (Controller)
        {
            Controller->StopMovement();
        }
    }

    return EBTNodeResult::Aborted;
}

