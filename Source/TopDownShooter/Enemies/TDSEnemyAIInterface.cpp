// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSEnemyAIInterface.h"

// Add default functionality here for any ITDSEnemyAIInterface functions that are not pure virtual.

FOnAttackEnd* ITDSEnemyAIInterface::GetOnAttackEndDelegate()
{
    return nullptr;
}

void ITDSEnemyAIInterface::Attack_Implementation()
{
}

float ITDSEnemyAIInterface::GetAttackRadius_Implementation() const
{
    return 0.0f;
}

bool ITDSEnemyAIInterface::CanAttack_Implementation() const
{
    return false;
}

bool ITDSEnemyAIInterface::CanMove_Implementation() const
{
    return false;
}

void ITDSBossAIInterface::Attack_Implementation()
{
}

float ITDSBossAIInterface::GetAttackRadius_Implementation() const
{
    return 0.0f;
}

bool ITDSBossAIInterface::CanAttack_Implementation() const
{
    return false;
}

bool ITDSBossAIInterface::CanMove_Implementation() const
{
    return false;
}

void ITDSBossAIInterface::SpecialAttack_Implementation()
{
}

float ITDSBossAIInterface::GetSpecialAttackRadius_Implementation() const
{
    return 0.0f;
}

bool ITDSBossAIInterface::CanUseSpecialAttack_Implementation() const
{
    return false;
}

FOnAttackEnd* ITDSBossAIInterface::GetOnAttackEndDelegate()
{
    return nullptr;
}
