// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDSEnemyAIInterface.generated.h"

UENUM()
enum class EAttackEndResult : uint8
{
	Succeed,
	Aborted
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAttackEnd, EAttackEndResult, Result);

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDSEnemyAIInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TOPDOWNSHOOTER_API ITDSEnemyAIInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Attack();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	float GetAttackRadius() const;
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	bool CanAttack() const;
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	bool CanMove() const;

	virtual FOnAttackEnd *GetOnAttackEndDelegate();

protected:
	virtual void Attack_Implementation();
	virtual float GetAttackRadius_Implementation() const;
	virtual bool CanAttack_Implementation() const;
	virtual bool CanMove_Implementation() const;
};

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDSBossAIInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 */
class TOPDOWNSHOOTER_API ITDSBossAIInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Attack();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	float GetAttackRadius() const;
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	bool CanAttack() const;
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	bool CanMove() const;

	virtual FOnAttackEnd* GetOnAttackEndDelegate();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SpecialAttack();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	float GetSpecialAttackRadius() const;
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	bool CanUseSpecialAttack() const;

protected:
	virtual void Attack_Implementation();
	virtual float GetAttackRadius_Implementation() const;
	virtual bool CanAttack_Implementation() const;
	virtual bool CanMove_Implementation() const;
	virtual void SpecialAttack_Implementation();
	virtual float GetSpecialAttackRadius_Implementation() const;
	virtual bool CanUseSpecialAttack_Implementation() const;
};
