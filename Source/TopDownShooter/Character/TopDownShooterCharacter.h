// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../FuncLib/TDSTypes.h"
#include "Weapon/TDSWeapon.h"
#include "../Items/TDSInteractable.h"
#include "../Effects/TDSAffectedActor.h"
#include "TopDownShooterCharacter.generated.h"

class UTDSCameraShake;
class UTDSStaminaSystem;

UCLASS(Blueprintable)
class ATopDownShooterCharacter : public ACharacter, public ITDSAffectedActor
{
	GENERATED_BODY()

public:
	ATopDownShooterCharacter();

	virtual void BeginPlay() override;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	virtual void NotifyActorEndOverlap(AActor* OtherActor) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	void SetSkinColor(const FLinearColor& Color);

protected:
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	void OnRep_PlayerState() override;
	void PossessedBy(AController* NewController) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void ReloadStart(ATDSWeapon* Weapon);
	void ReloadStart_Implementation(ATDSWeapon* Weapon);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void WeaponFireStart(ATDSWeapon* Weapon);
	void WeaponFireStart_Implementation(ATDSWeapon* Weapon);

	UFUNCTION(BlueprintCallable)
	void CameraShakeEvent() const;

	UFUNCTION(BlueprintCallable)
	void SetCameraHeight(float newCameraHeight);
	UFUNCTION(BlueprintCallable)
	void SetCameraRot(FRotator newCameraRot);
	UFUNCTION(BlueprintCallable)
	void SetCameraFlow(float newCameraLagSpeed, bool newIsFlow = true);

	UFUNCTION()
	void OnHeatlhChange(class UTDSHealthSystem* HealthSystem);

	UFUNCTION(BlueprintNativeEvent)
	void Death();
	void Death_Implementation();

private:
	FTransform StartMeshTransform;

	float ComputeChracterYawDelta(float DeltaSeconds);

	void SetupCurrentWeapon();
	void RemoveCurrentWeapon();

	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	UPROPERTY(EditDefaultsOnly, Category = Camera)
	TSubclassOf<UTDSCameraShake> CameraShakeClass;

	UPROPERTY()
	TScriptInterface<ITDSInteractable> InteractableItem;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ATDSPickUpWeapon> DropWeaponClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Gameplay, meta = (AllowPrivateAccess = "true"))
	class UAudioComponent* TakeDamageAudio;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Gameplay, meta = (AllowPrivateAccess = "true"))
	class USoundBase* TakeDamageSound;

	UPROPERTY()
	class UMaterialInstanceDynamic* SkinMaterial;
	UPROPERTY(EditDefaultsOnly)
	class UMaterialInterface* BloodDropMaterial;
	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* SwitchWeaponMontage;
	int CurWeaponInd;

	float AxisX = 0.f, AxisY = 0.f;

	UPROPERTY(ReplicatedUsing = OnRep_EffectsList)
	TArray<class UTDSStatusEffect*> EffectsList;
	UFUNCTION()
	void OnRep_EffectsList(const TArray<class UTDSStatusEffect*>& oldEffectsList) override { ITDSAffectedActor::OnRep_EffectsList(oldEffectsList); }

	UPROPERTY(EditDefaultsOnly)
	class UAnimMontage* StunAnimMontage;
	UPROPERTY(Replicated)
	bool isStuned = false;

	UFUNCTION()
	void OnAnimNotifyBegin(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload);
	UFUNCTION()
	void OnAnimMontageEnd(UAnimMontage* Montage, bool bInterrupted);

public:
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInteractableItemChange, const FString&, Str);
	UPROPERTY()
	FOnInteractableItemChange OnInteractableItemChange;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay, meta = (AllowPrivateAccess = "true"), ReplicatedUsing = OnRep_HealthSystem)
	class UTDSHealthSystem* HealthSystem;
	UFUNCTION()
	void OnRep_HealthSystem();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay, meta = (AllowPrivateAccess = "true"))
	UTDSStaminaSystem* StaminaSystem;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay, meta = (AllowPrivateAccess = "true"), ReplicatedUsing = OnRep_InventorySystem)
	class UTDSInventorySystem* InventorySystem;
	UFUNCTION()
	void OnRep_InventorySystem();
	UFUNCTION()
	void OnRep_WeaponList();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", ReplicatedUsing = CharacterUpdate)
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool IsAimEnable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool IsWalkEnable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool IsSprintEnable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;

	/*UPROPERTY(BlueprintReadWrite)
	ATDSWeapon *CurWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FName StartWeaponName;*/

	UFUNCTION()
	void InputAxisX(float val);
	UFUNCTION()
	void InputAxisY(float val);
	UFUNCTION()
	void FireStart();
	UFUNCTION()
	void FireEnd();
	UFUNCTION()
	void MovementTick(float DeltaSeconds);
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
	void UpdateMovementState();
	UFUNCTION(Server, unreliable, WithValidation)
	void UpdateMovementState_Server(EMovementState NewMovementState);

	UFUNCTION(BlueprintCallable)
	void ManualReload();

	UFUNCTION()
	void ChangeCurrentWeapon(int Index);
	UFUNCTION(BlueprintCallable)
	void StartSwitchingWeapon(int Index);
	UFUNCTION(BlueprintCallable, Server, reliable, WithValidation)
	void StartSwitchingWeapon_Server(int Index);
	void StartSwitchingWeapon_Server_Implementation(int Index);
	bool StartSwitchingWeapon_Server_Validate(int Index);
	UFUNCTION(BlueprintCallable, NetMulticast, reliable)
	void StartSwitchingWeapon_NetMulticast(int Index);
	void StartSwitchingWeapon_NetMulticast_Implementation(int Index);

	UFUNCTION(BlueprintImplementableEvent)
	void SetWeaponAnim(ATDSWeapon *Weapon);

	UFUNCTION(BlueprintCallable)
	void Interact();
	UFUNCTION(Server, Reliable, WithValidation)
	void Interact_Server();

	UFUNCTION(BlueprintCallable)
	int PickUpAmmo(EAmmoType Type, int Ammount);
	UFUNCTION(Server, Reliable, WithValidation)
	void AddAmmo_Server(EAmmoType Type, int Ammount);

	UFUNCTION(BlueprintCallable)
	bool PickUpWeapon(ATDSWeapon* Weapon);
	UFUNCTION(BlueprintCallable)
	void DropWeapon();
	UFUNCTION(Server, Reliable, WithValidation)
	void DropWeapon_Server();

	UFUNCTION(BlueprintCallable)
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser) override;
	UFUNCTION(NetMulticast, Unreliable)
	void TakeDamageCosmetics_NetMulticast();

	TArray<class UTDSStatusEffect*>& GetEffectsList() override { return EffectsList; }
	bool CanBeAffected() const override;
	void BeginStun() override;
	void EndStun() override;

	UFUNCTION(NetMulticast, Reliable)
	void Respawn_NetMulticast();
	UFUNCTION(Server, Reliable, WithValidation)
	void Respawn_Server();
};

