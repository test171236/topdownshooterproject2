// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/AudioComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/GameModeBase.h"
#include "Engine/DecalActor.h"
#include "Engine/EngineTypes.h"
#include "Engine/ActorChannel.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "../Game/TDSCameraShake.h"
#include "TDSStaminaSystem.h"
#include "TDSHealthSystem.h"
#include "TDSInventorySystem.h"
#include "../Game/TDSGameInstance.h"
#include "../Items/TDSPickUpWeapon.h"
#include "../Effects/TDSStatusEffect.h"
#include "../Game/TDSPlayerState.h"
#include "Net/UnrealNetwork.h"

ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	//CameraBoom->TargetArmLength = 800.f;
	//CameraBoom->SetRelativeRotation(FRotator(-90.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
	//CameraBoom->bEnableCameraLag = true;
	CameraBoom->CameraLagSpeed = 3.0f;
	//CameraBoom->bDrawDebugLagMarkers = true;

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	StaminaSystem = CreateDefaultSubobject<UTDSStaminaSystem>("StaminsSystem");
	HealthSystem = CreateDefaultSubobject<UTDSHealthSystem>("HealthSystem");
	HealthSystem->OnDeath.AddDynamic(this, &ATopDownShooterCharacter::Death);
	HealthSystem->OnHealthChange.AddDynamic(this, &ATopDownShooterCharacter::OnHeatlhChange);
	HealthSystem->SetIsReplicated(true);

	InventorySystem = CreateDefaultSubobject<UTDSInventorySystem>("InventorySystem");
	InventorySystem->SetIsReplicated(true);

	TakeDamageAudio = CreateDefaultSubobject<UAudioComponent>("TakeDamageAudio");
	TakeDamageAudio->SetupAttachment(RootComponent);
}

void ATopDownShooterCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Here we list the variables we want to replicate
	DOREPLIFETIME(ATopDownShooterCharacter, EffectsList);
	DOREPLIFETIME(ATopDownShooterCharacter, isStuned);
	DOREPLIFETIME_CONDITION(ATopDownShooterCharacter, MovementState, COND_SkipOwner);
	DOREPLIFETIME_CONDITION_NOTIFY(ATopDownShooterCharacter, InventorySystem, COND_InitialOnly, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(ATopDownShooterCharacter, HealthSystem, COND_InitialOnly, REPNOTIFY_Always);
}

void ATopDownShooterCharacter::OnRep_HealthSystem()
{
	/*if (HealthSystem)
	{
		HealthSystem->OnDeath.AddDynamic(this, &ATopDownShooterCharacter::Death);
		HealthSystem->OnHealthChange.AddDynamic(this, &ATopDownShooterCharacter::OnHeatlhChange);
	} */
}

void ATopDownShooterCharacter::OnRep_InventorySystem()
{
	if (InventorySystem)
	{
		InventorySystem->OnWeaponListReplicated.AddDynamic(this, &ATopDownShooterCharacter::OnRep_WeaponList);
	}
}

void ATopDownShooterCharacter::OnRep_WeaponList()
{
	ChangeCurrentWeapon(InventorySystem->GetCurrentWeaponIndex());
}

void ATopDownShooterCharacter::Death_Implementation()
{	
	/*if (IsLocallyControlled())
	{
		GetController()->UnPossess();
	} */

	if (HasAuthority())
	{
		if (InventorySystem->GetCurrentWeapon())
		{
			InventorySystem->GetCurrentWeapon()->AuthSetFiring(false);
		}
		while (EffectsList.Num() > 0)
		{
			EffectsList[0]->DestroyEffect();
		}
	}

	GetCapsuleComponent()->SetCollisionProfileName(TEXT("CharacterMesh"));
}

void ATopDownShooterCharacter::ChangeCurrentWeapon(int Index)
{
	RemoveCurrentWeapon();
	InventorySystem->SetCurrentWeaponIndex(Index);
	SetupCurrentWeapon();
}

void ATopDownShooterCharacter::StartSwitchingWeapon(int Index)
{
	if (!isStuned && !HealthSystem->GetIsDead())
	{
		CurWeaponInd = Index;
		PlayAnimMontage(SwitchWeaponMontage);
	}
}

void ATopDownShooterCharacter::StartSwitchingWeapon_Server_Implementation(int Index)
{
	StartSwitchingWeapon_NetMulticast(Index);
}
bool ATopDownShooterCharacter::StartSwitchingWeapon_Server_Validate(int Index)
{
	return true;
}

void ATopDownShooterCharacter::StartSwitchingWeapon_NetMulticast_Implementation(int Index)
{
	StartSwitchingWeapon(Index);
}

void ATopDownShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (!IsLocallyControlled())
	{
		CursorToWorld->DestroyComponent();
	}
	UpdateMovementState();

	StartMeshTransform = GetMesh()->GetRelativeTransform();

	UAnimInstance* anim_inst = GetMesh()->GetAnimInstance();
	if (anim_inst)
	{
		anim_inst->OnPlayMontageNotifyBegin.AddDynamic(this, &ATopDownShooterCharacter::OnAnimNotifyBegin);
		anim_inst->OnMontageEnded.AddDynamic(this, &ATopDownShooterCharacter::OnAnimMontageEnd);
	}

	SkinMaterial = GetMesh()->CreateAndSetMaterialInstanceDynamic(0);

	ATDSPlayerState* ps = Cast<ATDSPlayerState>(GetPlayerState());
	if (ps)
	{
		SetSkinColor(ps->Color);
	}

	if (GetLocalRole() == ENetRole::ROLE_Authority)
	{
		ChangeCurrentWeapon(0);
	}
}

void ATopDownShooterCharacter::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	ATDSPlayerState* ps = Cast<ATDSPlayerState>(GetPlayerState());
	if (ps)
	{
		SetSkinColor(ps->Color);
	}
}

void ATopDownShooterCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	ATDSPlayerState* ps = Cast<ATDSPlayerState>(GetPlayerState());
	if (ps)
	{
		SetSkinColor(ps->Color);
	}
}

void ATopDownShooterCharacter::SetSkinColor(const FLinearColor& Color)
{
	if (SkinMaterial)
	{
		SkinMaterial->SetVectorParameterValue(TEXT("BodyColor"), Color);
	}
}

void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	APlayerController * PC = Cast<APlayerController>(GetController());

	if (PC && PC->IsLocalController())
	{
		PC->SetAudioListenerOverride(nullptr, GetActorLocation(), TopDownCameraComponent->GetComponentRotation());

		if (!isStuned && !HealthSystem->GetIsDead())
		{
			MovementTick(DeltaSeconds);
		}

		if (CursorToWorld != nullptr)
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
}

void ATopDownShooterCharacter::SetupPlayerInputComponent(class UInputComponent* newInputComponent)
{
	Super::SetupPlayerInputComponent(newInputComponent);

	InputComponent = newInputComponent;

	InputComponent->BindAxis(TEXT("MoveForward"), this, &ATopDownShooterCharacter::InputAxisX);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &ATopDownShooterCharacter::InputAxisY);
	InputComponent->BindAction(TEXT("FireAction"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::FireStart);
	InputComponent->BindAction(TEXT("FireAction"), EInputEvent::IE_Released, this, &ATopDownShooterCharacter::FireEnd);
	InputComponent->BindAction(TEXT("ReloadAction"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::ManualReload);
}

void ATopDownShooterCharacter::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (OtherActor->GetClass()->ImplementsInterface(UTDSInteractable::StaticClass()))
	{
		InteractableItem = OtherActor;
		OnInteractableItemChange.Broadcast(InteractableItem->GetHUDText());
	}
}

void ATopDownShooterCharacter::NotifyActorEndOverlap(AActor* OtherActor)
{
	Super::NotifyActorEndOverlap(OtherActor);

	if (OtherActor == InteractableItem.GetObject())
	{
		InteractableItem = NULL;
		OnInteractableItemChange.Broadcast(TEXT(""));
	}
}

void ATopDownShooterCharacter::ManualReload()
{
	if (IsLocallyControlled() && Cast<APlayerController>(GetController()) && !isStuned && !HealthSystem->GetIsDead())
	{
		InventorySystem->ReloadWeapon_Server();
	}
}

void ATopDownShooterCharacter::SetCameraHeight(float newCameraHeight)
{
	CameraBoom->TargetArmLength = newCameraHeight;
}

void ATopDownShooterCharacter::SetCameraRot(FRotator newCameraRot)
{
	CameraBoom->SetRelativeRotation(newCameraRot);
}

void ATopDownShooterCharacter::SetCameraFlow(float newCameraLagSpeed, bool newIsFlow)
{
	CameraBoom->bEnableCameraLag = newIsFlow;
	CameraBoom->CameraLagSpeed = newCameraLagSpeed;
}

void ATopDownShooterCharacter::OnHeatlhChange(UTDSHealthSystem* HSys)
{
	if (IsValid(SkinMaterial))
	{
		SkinMaterial->SetScalarParameterValue(TEXT("Health"), HSys->GetCurHP() / HSys->GetMaxHP());
	}
}

void ATopDownShooterCharacter::SetupCurrentWeapon()
{
	ATDSWeapon* CurWeapon = InventorySystem->GetCurrentWeapon();

	if (CurWeapon)
	{
		FAttachmentTransformRules rule(EAttachmentRule::SnapToTarget, false);
		CurWeapon->AttachToComponent(GetMesh(), rule, FName("WeaponSocketRightHand"));
		CurWeapon->OnWeaponReloadStart.Clear();
		CurWeapon->OnWeaponReloadStart.AddDynamic(this, &ATopDownShooterCharacter::ReloadStart);
		CurWeapon->OnWeaponFireStart.Clear();
		CurWeapon->OnWeaponFireStart.AddDynamic(this, &ATopDownShooterCharacter::WeaponFireStart);
		SetWeaponAnim(CurWeapon);
		CurWeapon->Mesh->SetVisibility(true);
	}
}

void ATopDownShooterCharacter::RemoveCurrentWeapon()
{
	ATDSWeapon* CurWeapon = InventorySystem->GetCurrentWeapon();

	if (CurWeapon)
	{
		CurWeapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		CurWeapon->OnWeaponReloadStart.Clear();
		CurWeapon->OnWeaponFireStart.Clear();
		CurWeapon->Mesh->SetVisibility(false);
	}
}

void ATopDownShooterCharacter::OnAnimNotifyBegin(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
	if (NotifyName == TEXT("ChangeWeapon"))
	{
		ChangeCurrentWeapon(CurWeaponInd);
	}
}

void ATopDownShooterCharacter::OnAnimMontageEnd(UAnimMontage* Montage, bool bInterrupted)
{
	if (isStuned && Montage == StunAnimMontage)
	{
		PlayAnimMontage(StunAnimMontage);
	}
}

void ATopDownShooterCharacter::InputAxisX(float val)
{
	AxisX = val;
}

void ATopDownShooterCharacter::InputAxisY(float val)
{
	AxisY = val;
}

float ATopDownShooterCharacter::ComputeChracterYawDelta(float DeltaSeconds)
{
	APlayerController* my_controller = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (!IsValid(my_controller))
	{
		return 0;
	}
	
	float TargetYaw = 0;
	
	if (MovementState == EMovementState::Sprint_State && (abs(AxisX) > 0.1f || abs(AxisY) > 0.1f))
	{
		TargetYaw = atan2f(AxisY, AxisX) * 180.f / PI;
	}
	else 
	{
		FHitResult hr;
		my_controller->GetHitResultUnderCursor(ECC_GameTraceChannel1, false, hr);
		FRotator rot = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), hr.Location);
		TargetYaw = rot.Yaw;
	}

	const float CharacterRotateSpeedDegreeInSecond = 360.f;

	float delta = fmod(TargetYaw - GetActorRotation().Yaw, 360.f);
	if (delta > 180.f)
	{
		delta -= 360.f;
	}
	else if (delta < -180.f)
	{
		delta += 360.f;
	}
	/*float add_delta = CharacterRotateSpeedDegreeInSecond * DeltaSeconds * UKismetMathLibrary::SignOfFloat(delta);

	if (abs(add_delta) <= abs(delta))
		return add_delta;
	else*/
	return delta;
}

void ATopDownShooterCharacter::MovementTick(float DeltaSeconds)
{
	if (MovementState == EMovementState::Sprint_State && (abs(AxisX) > 0.1f || abs(AxisY) > 0.1f))
	{
		if (!StaminaSystem->SpendStamina(DeltaSeconds * 20.f))
		{
			IsSprintEnable = false;
		}
	}
	UpdateMovementState();

	AddMovementInput(FVector(1.f, 0.f, 0.f), AxisX);
	AddMovementInput(FVector(0.f, 1.f, 0.f), AxisY);
	
	AddControllerYawInput(ComputeChracterYawDelta(DeltaSeconds) / 10);
	/*SetActorRotation(FQuat(FRotator(0.f,
		GetActorRotation().Yaw + ComputeChracterYawDelta(DeltaSeconds), 0.f)));	*/
}

void ATopDownShooterCharacter::CharacterUpdate()
{
	float res_speed = 600.f;
	
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		res_speed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		res_speed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		res_speed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		res_speed = MovementInfo.SprintSpeed;
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = res_speed;
}

void ATopDownShooterCharacter::FireStart()
{
	if (InventorySystem->GetCurrentWeapon() && !isStuned && !HealthSystem->GetIsDead())
	{
		InventorySystem->GetCurrentWeapon()->SetFiring_Server(true);
	}
}

void ATopDownShooterCharacter::FireEnd()
{
	if (InventorySystem->GetCurrentWeapon() && !isStuned && !HealthSystem->GetIsDead())
	{
		InventorySystem->GetCurrentWeapon()->SetFiring_Server(false);
	}
}

void ATopDownShooterCharacter::UpdateMovementState()
{
	EMovementState NewMovementState = EMovementState::Run_State;

	if (IsSprintEnable)
		NewMovementState = EMovementState::Sprint_State;
	else if (IsAimEnable)
		NewMovementState = EMovementState::Aim_State;
	else if (IsWalkEnable)
		NewMovementState = EMovementState::Walk_State;
	else
		NewMovementState = EMovementState::Run_State;

	if (MovementState != NewMovementState)
	{
		MovementState = NewMovementState;
		UpdateMovementState_Server(NewMovementState);
	}

	CharacterUpdate();
}

void ATopDownShooterCharacter::UpdateMovementState_Server_Implementation(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();
}

bool ATopDownShooterCharacter::UpdateMovementState_Server_Validate(EMovementState NewMovementState)
{
	return true;
}

void ATopDownShooterCharacter::ReloadStart_Implementation(ATDSWeapon* Weapon)
{
	PlayAnimMontage(InventorySystem->GetCurrentWeapon()->Settings.ReloadAnim);
}

void ATopDownShooterCharacter::WeaponFireStart_Implementation(ATDSWeapon* Weapon)
{
	if (MovementState == EMovementState::Aim_State)
	{
		PlayAnimMontage(InventorySystem->GetCurrentWeapon()->Settings.FireAimAnim);
	}
	else
	{
		PlayAnimMontage(InventorySystem->GetCurrentWeapon()->Settings.FireWalkAnim);
	}
}

void ATopDownShooterCharacter::CameraShakeEvent(void) const
{
	GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayWorldCameraShake(GetWorld(), CameraShakeClass, 
		TopDownCameraComponent->GetComponentLocation(), 1.f, 1.f, 1.f);
}

void ATopDownShooterCharacter::Interact(void)
{
	if (IsLocallyControlled() && InteractableItem && !isStuned)
	{
		Interact_Server();
	}
}
void ATopDownShooterCharacter::Interact_Server_Implementation(void)
{
	if (InteractableItem && (Cast<APlayerController>(GetController())) && !isStuned)
	{
		InteractableItem->Interact(this);
	}
}
bool ATopDownShooterCharacter::Interact_Server_Validate(void)
{
	return true;
}

int ATopDownShooterCharacter::PickUpAmmo(EAmmoType Type, int Ammount)
{
	if (IsLocallyControlled())
	{
		int res = InventorySystem->PickUpAmmo(Type, Ammount);
		if (GetLocalRole() != ENetRole::ROLE_Authority)
		{
			AddAmmo_Server(Type, Ammount);
		}
		return res;
	}
	return 0;
}

void ATopDownShooterCharacter::AddAmmo_Server_Implementation(EAmmoType Type, int Ammount)
{
	InventorySystem->PickUpAmmo(Type, Ammount);
}
bool ATopDownShooterCharacter::AddAmmo_Server_Validate(EAmmoType Type, int Ammount)
{
	return true;
}

bool ATopDownShooterCharacter::PickUpWeapon(ATDSWeapon* Weapon)
{
	if (IsValid(InventorySystem->WeaponList[InventorySystem->GetCurrentWeaponIndex()]) || isStuned)
	{
		return false;
	}

	InventorySystem->SetWeapon(InventorySystem->GetCurrentWeaponIndex(), Weapon);
	ChangeCurrentWeapon(InventorySystem->GetCurrentWeaponIndex());

	return true;
}

float ATopDownShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float res = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	res = HealthSystem->ReduceHP(res);
	if (res > 0 && HealthSystem->GetCurShield() <= 0)
	{
		TakeDamageCosmetics_NetMulticast();
	}

	return res;
}

void ATopDownShooterCharacter::TakeDamageCosmetics_NetMulticast_Implementation()
{
	if (FMath::FRand() < 0.3)
	{
		FHitResult hr;
		FVector dir = FVector(0, 0, -1).RotateAngleAxis(FMath::FRandRange(0, 45), FVector(1, 0, 0)).RotateAngleAxis(FMath::FRandRange(0, 360), FVector(0, 0, 1));

		UKismetSystemLibrary::LineTraceSingle(GetWorld(),
			GetActorLocation(),
			GetActorLocation() + dir * 1000.f,
			UEngineTypes::ConvertToTraceType(ECC_GameTraceChannel1),
			false,
			TArray<AActor*>({ this }),
			EDrawDebugTrace::None,
			hr,
			true);

		if (hr.bBlockingHit)
		{
			FRotator rot = hr.ImpactNormal.RotateAngleAxis(90, FVector(0, 1, 0)).Rotation();
			rot.Yaw = FMath::FRandRange(0, 360);

			ADecalActor* da = GetWorld()->SpawnActor<ADecalActor>(ADecalActor::StaticClass(),
				FTransform(rot, hr.ImpactPoint));

			da->GetDecal()->DecalSize = FVector(1, 100, 100);
			da->GetDecal()->SetMaterial(0, BloodDropMaterial);
		}
	}

	if (!TakeDamageAudio->IsPlaying())
	{
		TakeDamageAudio->Sound = TakeDamageSound;
		TakeDamageAudio->Play();
	}
}

bool ATopDownShooterCharacter::CanBeAffected() const
{
	return !HealthSystem->GetIsDead();
}

void ATopDownShooterCharacter::BeginStun()
{
	isStuned = true;

	PlayAnimMontage(StunAnimMontage);

}

void ATopDownShooterCharacter::EndStun()
{
	isStuned = false;

	StopAnimMontage(StunAnimMontage);
}

void ATopDownShooterCharacter::DropWeapon()
{
	if (!IsValid(InventorySystem->WeaponList[InventorySystem->GetCurrentWeaponIndex()]) || !(Cast<APlayerController>(GetController())) || isStuned || !IsLocallyControlled())
	{
		return;
	}

	DropWeapon_Server();
}

void ATopDownShooterCharacter::DropWeapon_Server_Implementation()
{
	if (!IsValid(InventorySystem->WeaponList[InventorySystem->GetCurrentWeaponIndex()]) || !(Cast<APlayerController>(GetController())) || isStuned)
	{
		return;
	}

	ATDSPickUpWeapon* item = GetWorld()->SpawnActor<ATDSPickUpWeapon>(DropWeaponClass,
		FTransform(GetActorLocation()));

	if (IsValid(item))
	{
		item->InitWeapon(InventorySystem->RemoveWeapon(InventorySystem->GetCurrentWeaponIndex()));
		ChangeCurrentWeapon(InventorySystem->GetCurrentWeaponIndex());
	}
}
bool ATopDownShooterCharacter::DropWeapon_Server_Validate()
{
	return true;
}


void ATopDownShooterCharacter::Respawn_NetMulticast_Implementation()
{
	StopAnimMontage();
	GetMesh()->SetCollisionProfileName(TEXT("CharacterMesh"));
	GetCapsuleComponent()->SetCollisionProfileName(TEXT("Pawn"));
	GetMesh()->SetAllBodiesSimulatePhysics(false);
	GetMesh()->AttachToComponent(GetCapsuleComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	GetMesh()->SetRelativeTransform(StartMeshTransform);
}

void ATopDownShooterCharacter::Respawn_Server_Implementation()
{
	ATDSPlayerState* ps = Cast<ATDSPlayerState>(GetPlayerState());
	if (ps && ps->RespawnPoint)
	{
		SetActorLocation(ps->RespawnPoint->GetActorLocation());
	}
	else if (GetWorld()->GetAuthGameMode())
	{
		SetActorLocation(GetWorld()->GetAuthGameMode()->ChoosePlayerStart(GetController())->GetActorLocation());
	}

	HealthSystem->RestoreFullHP();

	Respawn_NetMulticast();
}

bool ATopDownShooterCharacter::Respawn_Server_Validate()
{
	return true;
}

bool ATopDownShooterCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (auto effect : EffectsList)
	{
		if (effect)
		{
			WroteSomething |= Channel->ReplicateSubobject(effect, *Bunch, *RepFlags);
		}
	}
	return WroteSomething;
}
