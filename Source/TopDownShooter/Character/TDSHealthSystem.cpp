// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSHealthSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Net/UnrealNetwork.h"
#include "../Widgets/TDSDamageDisplayActor.h"

UTDSHealthSystem::UTDSHealthSystem()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}


void UTDSHealthSystem::BeginPlay()
{
	Super::BeginPlay();

	CurHP = MaxHP;
	CurShield = MaxShield;

	if (IsValid(GetOwner()))
	{
		ShieldFXComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(ShieldFX, GetOwner()->GetRootComponent(), FName(""),
			FVector::ZeroVector, FRotator(), EAttachLocation::SnapToTarget, true);
	}
}

void UTDSHealthSystem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Here we list the variables we want to replicate
	DOREPLIFETIME(UTDSHealthSystem, MaxHP);
	DOREPLIFETIME(UTDSHealthSystem, CurHP);
	DOREPLIFETIME(UTDSHealthSystem, isDead);
	DOREPLIFETIME(UTDSHealthSystem, MaxShield);
	DOREPLIFETIME(UTDSHealthSystem, CurShield);
}

void UTDSHealthSystem::OnRep_MaxHP()
{
	OnHealthChange.Broadcast(this);
}

void UTDSHealthSystem::OnRep_CurHP()
{
	OnHealthChange.Broadcast(this);
}

void UTDSHealthSystem::OnRep_isDead(bool oldIsDead)
{
	if (isDead && !oldIsDead)
	{
		OnDeath.Broadcast();
	}
}

void UTDSHealthSystem::OnRep_MaxShield()
{
	if (IsValid(ShieldFXComponent))
	{
		ShieldFXComponent->SetFloatParameter("Health", CurShield / MaxShield);
	}

	OnHealthChange.Broadcast(this);
}

void UTDSHealthSystem::OnRep_CurShield(float oldCurShield)
{
	if (IsValid(ShieldFXComponent))
	{
		ShieldFXComponent->SetFloatParameter("Health", CurShield / MaxShield);
		if (CurShield <= 0 && oldCurShield > 0)
		{
			ShieldFXComponent->SetVisibility(false);
		}
		if (oldCurShield <= 0 && CurShield > 0)
		{
			ShieldFXComponent->SetVisibility(true);
		}
	}

	OnHealthChange.Broadcast(this);
}

void UTDSHealthSystem::StartShieldRecovery()
{
	SetComponentTickEnabled(true);
}

void UTDSHealthSystem::SetMaxHP(float newMaxHP)
{
	MaxHP = newMaxHP;
	CurHP = newMaxHP;
	isDead = false;
	OnHealthChange.Broadcast(this);
}

void UTDSHealthSystem::ChangeMaxHP(float newMaxHP)
{
	MaxHP = newMaxHP;
	if (CurHP > MaxHP)
	{
		CurHP = MaxHP;
	}
	OnHealthChange.Broadcast(this);
}

void UTDSHealthSystem::SetCurrentHP(float newcurHP)
{
	CurHP = newcurHP;
	OnHealthChange.Broadcast(this);
	if (newcurHP > 0)
	{
		isDead = false;
	}
	else
	{
		isDead = true;
		//OnDeath.Broadcast();
	}
}

float UTDSHealthSystem::ReduceHP(float deltaHP)
{
	if (isDead || deltaHP <= 0.f || isInvincible)
	{
		return 0.0f;
	}

	float ShieldDamage = 0;
	float HPDamage = 0;

	ShieldDamage = FMath::Min(deltaHP, CurShield);
	CurShield -= ShieldDamage;
	SetComponentTickEnabled(false);
	GetWorld()->GetTimerManager().SetTimer(ShieldRecoveryTimer,
		this, &UTDSHealthSystem::StartShieldRecovery, ShieldRegenTimeDelay, false);
	if (IsValid(ShieldFXComponent))
	{
		ShieldFXComponent->SetFloatParameter("Health", CurShield / MaxShield);
		if (CurShield <= 0)
		{
			ShieldFXComponent->SetVisibility(false);// Deactivate();
		}
	}

	HPDamage = FMath::Min(deltaHP - ShieldDamage, CurHP);
	CurHP -= HPDamage;
	OnHealthChange.Broadcast(this);

	SpawnDamageWidget_NetMulticast(ShieldDamage, HPDamage);

	if (CurHP <= 0.f)
	{
		isDead = true;
		OnDeath.Broadcast();
	}

	return ShieldDamage + HPDamage;
}

void UTDSHealthSystem::SpawnDamageWidget_NetMulticast_Implementation(float ShieldDamage, float HPDamage)
{
	if (IsValid(GetOwner()))
	{
		ATDSDamageDisplayActor* dda = GetWorld()->SpawnActor<ATDSDamageDisplayActor>(DamageDisplayActorClass, GetOwner()->GetTransform());
		dda->ChangeText(ShieldDamage + HPDamage, ShieldDamage > 0.f, CurHP / MaxHP);
	}
}

float UTDSHealthSystem::AddHP(float deltaHP)
{
	if (isDead || deltaHP <= 0.f)
	{
		return 0.0f;
	}

	float res;
	if (CurHP + deltaHP >= MaxHP)
	{
		res = MaxHP - CurHP;
		CurHP = MaxHP;
		OnHealthChange.Broadcast(this);
		return res;
	}
	else
	{
		res = deltaHP;
		CurHP += deltaHP;
		OnHealthChange.Broadcast(this);
		return res;
	}
}

float UTDSHealthSystem::AddShield(float deltaShield)
{
	if (isDead || deltaShield <= 0.f)
	{
		return 0.0f;
	}

	bool ActivateShield = (CurShield <= 0.f);
	float ShieldRecovery = 0;
	ShieldRecovery = FMath::Min(deltaShield, MaxShield - CurShield);
	CurShield += ShieldRecovery;
	OnHealthChange.Broadcast(this);
	if (IsValid(ShieldFXComponent))
	{
		ShieldFXComponent->SetFloatParameter("Health", CurShield / MaxShield);
		if (ActivateShield)
		{
			ShieldFXComponent->SetVisibility(true);//Activate(true);
		}
	}

	return ShieldRecovery;
}

void UTDSHealthSystem::RestoreFullHP()
{
	isDead = false;
	CurHP = MaxHP;
	CurShield = MaxShield;
	OnHealthChange.Broadcast(this);
	if (IsValid(ShieldFXComponent))
	{
		ShieldFXComponent->SetFloatParameter("Health", CurShield / MaxShield);
		ShieldFXComponent->SetVisibility(true);
	}
}

float UTDSHealthSystem::GetCurHP()	const
{
	return CurHP;
}

float UTDSHealthSystem::GetMaxHP()	const
{
	return MaxHP;
}

float UTDSHealthSystem::GetCurShield() const
{
	return CurShield;
}

float UTDSHealthSystem::GetMaxShield() const
{
	return MaxShield;
}

bool UTDSHealthSystem::GetIsDead() const
{
	return isDead;
}

void UTDSHealthSystem::SetIsInvincible(bool isInv)
{
	isInvincible = isInv;
}

void UTDSHealthSystem::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	AddShield(DeltaTime * ShieldRegenRate);
	if (CurShield >= MaxShield)
	{
		SetComponentTickEnabled(false);
	}
}

