// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSInventorySystem.h"

#include "Net/UnrealNetwork.h"
#include "../Game/TDSGameInstance.h"
#include "Weapon/TDSWeapon.h"

UTDSInventorySystem::UTDSInventorySystem()
{
	PrimaryComponentTick.bCanEverTick = false;

	WeaponList.Empty();
	WeaponList.AddZeroed(WEAPON_SLOT_NUM);

	AmmoAmount.Empty();
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EAmmoType"), true);
	if (EnumPtr)
	{
		for (uint8 i = 0; i < EnumPtr->GetMaxEnumValue(); i++)
		{
			AmmoAmount.Add(0);
		}
	}
}

void UTDSInventorySystem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Here we list the variables we want to replicate
	DOREPLIFETIME(UTDSInventorySystem, AmmoAmount);
	DOREPLIFETIME(UTDSInventorySystem, NoWeapon);
	DOREPLIFETIME(UTDSInventorySystem, WeaponList);
}

void UTDSInventorySystem::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner()->GetLocalRole() == ENetRole::ROLE_Authority)
	{
		InitWeapon(NoWeapon, TEXT("NoWeapon"));

		for (int i = 0; i < (StartWeaponList.Num() < WEAPON_SLOT_NUM ? StartWeaponList.Num() : WEAPON_SLOT_NUM); i++)
		{
			InitWeapon(WeaponList[i], StartWeaponList[i]);
			OnUpdateWeaponSlot.Broadcast(i, WeaponList[i]->Settings.Icon);
		}

		OnUpdateAmmoSlots.Broadcast(this);
	}
}

void UTDSInventorySystem::InitWeapon_Server_Implementation(int Ind, FName WeaponName)
{
	if (Ind == -1)
	{
		InitWeapon(NoWeapon, WeaponName);
	}
	else
	{
		InitWeapon(WeaponList[Ind], WeaponName);
	}
}
bool UTDSInventorySystem::InitWeapon_Server_Validate(int Ind, FName WeaponName)
{
	return true;
}

void UTDSInventorySystem::InitWeapon(ATDSWeapon*& Weapon, FName WeaponName)
{
	if (!IsValid(GetOwner()))
	{
		return;
	}

	UTDSGameInstance* inst = Cast<UTDSGameInstance>(GetOwner()->GetGameInstance());
	FWeaponInfo info;

	if (inst && inst->GetWeaponInfoByName(WeaponName, info))
	{
		if (IsValid(Weapon))
		{
			Weapon->Destroy();
		}

		FActorSpawnParameters asp;
		asp.Owner = GetOwner();
		asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		Weapon = GetWorld()->SpawnActor<ATDSWeapon>(info.WeaponClass, asp);
		if (Weapon)
		{
			Weapon->InitWeapon(info);
			Weapon->OnCurRoundsChanged.AddDynamic(this, &UTDSInventorySystem::UpdateAmmoSlots);
			Weapon->OutOfAmmo_Auth.AddDynamic(this, &UTDSInventorySystem::AuthReloadWeapon);
			//InitWeapon_NetMulticast(Weapon, info);
		}
	}
}

void UTDSInventorySystem::InitWeapon_NetMulticast_Implementation(ATDSWeapon* Weapon, const FWeaponInfo& Info)
{
	Weapon->InitWeapon(Info);
	Weapon->OnCurRoundsChanged.AddDynamic(this, &UTDSInventorySystem::UpdateAmmoSlots);
}

void UTDSInventorySystem::OnRep_CurrentWeaponIndex()
{
	SetCurrentWeaponIndex(CurrentWeaponIndex);
}

void UTDSInventorySystem::OnRep_NoWeapon(ATDSWeapon* OldNoWeapon)
{
	UTDSGameInstance* inst = Cast<UTDSGameInstance>(GetOwner()->GetGameInstance());
	FWeaponInfo info;

	if (inst && NoWeapon && inst->GetWeaponInfoByClass(NoWeapon->GetClass(), info))
	{
		NoWeapon->InitWeapon(info);
	}
}

void UTDSInventorySystem::OnRep_WeaponList(const TArray<class ATDSWeapon*>& OldWeaponList)
{
	for (int i = 0; i < WeaponList.Num(); i++)
	{
		if (WeaponList[i])
		{
			WeaponList[i]->OnCurRoundsChanged.Clear();
			WeaponList[i]->OnCurRoundsChanged.AddDynamic(this, &UTDSInventorySystem::UpdateAmmoSlots);
			OnUpdateWeaponSlot.Broadcast(i, WeaponList[i]->Settings.Icon);
		}
		else
		{
			OnUpdateWeaponSlot.Broadcast(i, FSoftObjectPath());
		}
	}
	OnUpdateAmmoSlots.Broadcast(this);
	OnWeaponListReplicated.Broadcast();
}

void UTDSInventorySystem::OnRep_AmmoAmount()
{
	OnUpdateAmmoSlots.Broadcast(this);
}

void UTDSInventorySystem::UpdateAmmoSlots(ATDSWeapon* Weapon)
{
	OnUpdateAmmoSlots.Broadcast(this);
}

ATDSWeapon* UTDSInventorySystem::GetCurrentWeapon() const
{
	if (IsValid(WeaponList[CurrentWeaponIndex]))
	{
		return WeaponList[CurrentWeaponIndex];
	}
	else
	{
		return NoWeapon;
	}
}

void UTDSInventorySystem::SetCurrentWeaponIndex(int Ind)
{
	CurrentWeaponIndex = Ind;

	OnCurrentWeaponIndexUpdate.Broadcast(Ind);
}

void UTDSInventorySystem::AuthReloadWeapon()
{
	if (GetCurrentWeapon()->GetCurrentRounds() < GetCurrentWeapon()->Settings.MaxRounds
		&& AmmoAmount[(int)GetCurrentWeapon()->GetCurrentAmmoType()] > 0)
	{
		GetCurrentWeapon()->AuthReloadStart(&AmmoAmount[(int)GetCurrentWeapon()->GetCurrentAmmoType()]);
	}
}

void UTDSInventorySystem::ReloadWeapon_Server_Implementation()
{
	AuthReloadWeapon();
}

bool UTDSInventorySystem::ReloadWeapon_Server_Validate()
{
	return true;
}

void UTDSInventorySystem::ReloadWeapon_NetMulticast_Implementation()
{
	AuthReloadWeapon();
}

void UTDSInventorySystem::SetWeapon(int Ind, ATDSWeapon* Weapon)
{
	if (IsValid(WeaponList[Ind]))
	{
		WeaponList[Ind]->Destroy();
	}

	WeaponList[Ind] = Weapon;
	Weapon->SetOwner(GetOwner());
	Weapon->OnCurRoundsChanged.Clear();
	Weapon->OnCurRoundsChanged.AddDynamic(this, &UTDSInventorySystem::UpdateAmmoSlots);
	Weapon->OutOfAmmo_Auth.Clear();
	Weapon->OutOfAmmo_Auth.AddDynamic(this, &UTDSInventorySystem::AuthReloadWeapon);
	OnUpdateWeaponSlot.Broadcast(Ind, Weapon->Settings.Icon);
	OnUpdateAmmoSlots.Broadcast(this);
}

ATDSWeapon* UTDSInventorySystem::RemoveWeapon(int Ind)
{
	if (!IsValid(WeaponList[Ind]))
	{
		return nullptr;
	}

	ATDSWeapon* Weapon = WeaponList[Ind];
	WeaponList[Ind] = nullptr;
	Weapon->OnCurRoundsChanged.Clear();
	Weapon->OnWeaponReloadStart.Clear();
	Weapon->OnWeaponFireStart.Clear();
	Weapon->OutOfAmmo_Auth.Clear();
	OnUpdateWeaponSlot.Broadcast(Ind, NoWeapon->Settings.Icon);
	OnUpdateAmmoSlots.Broadcast(this);

	return Weapon;
}

int UTDSInventorySystem::PickUpAmmo(EAmmoType Type, int Ammount)
{
	AmmoAmount[(int)Type] += Ammount;
	OnUpdateAmmoSlots.Broadcast(this);

	return Ammount;
}
