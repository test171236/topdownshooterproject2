// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../FuncLib/TDSTypes.h"
#include "TDSInventorySystem.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWNSHOOTER_API UTDSInventorySystem : public UActorComponent
{
	GENERATED_BODY()

public:	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponListReplicated);
	UPROPERTY()
	FOnWeaponListReplicated OnWeaponListReplicated;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCurrentWeaponIndexUpdate, int, Ind);
	UPROPERTY()
	FOnCurrentWeaponIndexUpdate OnCurrentWeaponIndexUpdate;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlot, int, Ind, const FSoftObjectPath&, Path);
	UPROPERTY()
	FOnUpdateWeaponSlot OnUpdateWeaponSlot;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateAmmoSlots, UTDSInventorySystem*, InventorySystem);
	UPROPERTY()
	FOnUpdateAmmoSlots OnUpdateAmmoSlots;

	UTDSInventorySystem();

	const int WEAPON_SLOT_NUM = 4;

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing = OnRep_NoWeapon)
	class ATDSWeapon* NoWeapon;
	UFUNCTION()
	void OnRep_NoWeapon(ATDSWeapon* OldNoWeapon);

	UPROPERTY(EditDefaultsOnly)
	TArray<FName> StartWeaponList;

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing = OnRep_WeaponList)
	TArray<class ATDSWeapon *> WeaponList;
	UFUNCTION()
	void OnRep_WeaponList(const TArray<class ATDSWeapon*> &OldWeaponList);

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing = OnRep_AmmoAmount)
	TArray<int> AmmoAmount;
	UFUNCTION()
	void OnRep_AmmoAmount();


protected:
	UPROPERTY(BlueprintReadWrite, ReplicatedUsing = OnRep_CurrentWeaponIndex)
	int CurrentWeaponIndex = 0;
	UFUNCTION()
	void OnRep_CurrentWeaponIndex();

	virtual void BeginPlay() override;
	void InitWeapon(class ATDSWeapon*& Weapon, FName WeaponName);

	UFUNCTION(Server, reliable, WithValidation)
	void InitWeapon_Server(int Ind, FName WeaponName);
	UFUNCTION(NetMulticast, reliable)
	void InitWeapon_NetMulticast(ATDSWeapon* Weapon, const FWeaponInfo &Info);

	UFUNCTION()
	void UpdateAmmoSlots(class ATDSWeapon* Weapon);
public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	ATDSWeapon* GetCurrentWeapon() const;

	UFUNCTION(BlueprintCallable)
	void SetCurrentWeaponIndex(int Ind);
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetCurrentWeaponIndex() const { return CurrentWeaponIndex; }

	UFUNCTION(BlueprintCallable)
	void AuthReloadWeapon();
	UFUNCTION(Server, reliable, WithValidation)
	void ReloadWeapon_Server();
	UFUNCTION(NetMulticast, reliable)
	void ReloadWeapon_NetMulticast();
	void SetWeapon(int Ind, ATDSWeapon* Weapon);
	ATDSWeapon* RemoveWeapon(int Ind);

	int PickUpAmmo(EAmmoType Type, int Ammount);
};
