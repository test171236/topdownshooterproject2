// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSHealthSystem.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWNSHOOTER_API UTDSHealthSystem : public UActorComponent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_MaxHP)
	float MaxHP = 100.f;
	UFUNCTION()
	void OnRep_MaxHP();
	UPROPERTY(ReplicatedUsing = OnRep_CurHP)
	float CurHP = 100.f;
	UFUNCTION()
	void OnRep_CurHP();

	UPROPERTY(ReplicatedUsing = OnRep_isDead)
	bool isDead = false;
	UFUNCTION()
	void OnRep_isDead(bool oldIsDead);

	bool isInvincible = false;

	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_MaxShield)
	float MaxShield = 10.f;
	UFUNCTION()
	void OnRep_MaxShield();
	
	UPROPERTY(ReplicatedUsing = OnRep_CurShield)
	float CurShield = 10.f;
	UFUNCTION()
	void OnRep_CurShield(float oldCurShield);
	
	UPROPERTY(EditAnywhere)
	float ShieldRegenRate = 1.f;
	UPROPERTY(EditAnywhere)
	float ShieldRegenTimeDelay = 3.f;

	FTimerHandle ShieldRecoveryTimer;

	UPROPERTY(EditAnywhere)
	class UNiagaraSystem* ShieldFX;
	UPROPERTY(EditAnywhere)
	class UNiagaraComponent* ShieldFXComponent;
	UPROPERTY(EditAnywhere)
	TSubclassOf<class ATDSDamageDisplayActor> DamageDisplayActorClass;

public:
	UTDSHealthSystem();

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void StartShieldRecovery();

public:
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);
	UPROPERTY(BlueprintAssignable)
	FOnDeath	OnDeath;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHealthChange, UTDSHealthSystem*, HealthSystem);
	UPROPERTY(BlueprintAssignable)
	FOnHealthChange	OnHealthChange;

	UFUNCTION(BlueprintCallable)
	void SetMaxHP(float newMaxHP);

	UFUNCTION(BlueprintCallable)
	void ChangeMaxHP(float newMaxHP);

	UFUNCTION(BlueprintCallable)
	void SetCurrentHP(float newCurHP);

	UFUNCTION(BlueprintCallable)
	float ReduceHP(float deltaHP);
	UFUNCTION(NetMulticast, Unreliable)
	void SpawnDamageWidget_NetMulticast(float ShieldDamage, float HPDamage);

	UFUNCTION(BlueprintCallable)
	float AddHP(float deltaHP);
	UFUNCTION(BlueprintCallable)
	float AddShield(float deltaShield);

	UFUNCTION(BlueprintCallable)
	void RestoreFullHP();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCurHP() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetMaxHP() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCurShield() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetMaxShield() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetIsDead() const;

	void SetIsInvincible(bool isInv);

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
