// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../../FuncLib/TDSTypes.h"
#include "TDSExplosion.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSExplosion : public AActor
{
	GENERATED_BODY()
	
	UPROPERTY()
	FExplosionInfo ExplosionInfo;

	FHitResult HitRes;

	bool ApplyExplosionDamage();

	UFUNCTION(NetMulticast, Unreliable)
	void SpawnCosmetics_NetMulticast(UParticleSystem* ExplosionParticle, USoundBase* Sound);

public:	
	ATDSExplosion();

	UPROPERTY(EditAnywhere)
	bool IsDebug = false;

protected:
	virtual void BeginPlay() override;

public:	
	void InitExplosion(const FExplosionInfo& ExpInf, const FHitResult& HR);

};
