// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSExplosion.h"

#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "../../Effects/TDSStatusEffect.h"

static bool ComponentIsDamageableFrom(UPrimitiveComponent* VictimComp, FVector const& Origin, AActor const* IgnoredActor, const TArray<AActor*>& IgnoreActors, ECollisionChannel TraceChannel, FHitResult& OutHitResult)
{
	FCollisionQueryParams LineParams(SCENE_QUERY_STAT(ComponentIsVisibleFrom), true, IgnoredActor);
	LineParams.AddIgnoredActors(IgnoreActors);

	// Do a trace from origin to middle of box
	UWorld* const World = VictimComp->GetWorld();
	check(World);

	FVector const TraceEnd = VictimComp->Bounds.Origin;
	FVector TraceStart = Origin;
	if (Origin == TraceEnd)
	{
		// tiny nudge so LineTraceSingle doesn't early out with no hits
		TraceStart.Z += 0.01f;
	}

	// Only do a line trace if there is a valid channel, if it is invalid then result will have no fall off
	if (TraceChannel != ECollisionChannel::ECC_MAX)
	{
		bool const bHadBlockingHit = World->LineTraceSingleByChannel(OutHitResult, TraceStart, TraceEnd, TraceChannel, LineParams);
		//::DrawDebugLine(World, TraceStart, TraceEnd, FLinearColor::Red, true);

		// If there was a blocking hit, it will be the last one
		if (bHadBlockingHit)
		{
			if (OutHitResult.Component == VictimComp)
			{
				// if blocking hit was the victim component, it is visible
				return true;
			}
			else
			{
				// if we hit something else blocking, it's not
				UE_LOG(LogDamage, Log, TEXT("Radial Damage to %s blocked by %s (%s)"), *GetNameSafe(VictimComp), *GetNameSafe(OutHitResult.GetActor()), *GetNameSafe(OutHitResult.Component.Get()));
				return false;
			}
		}
	}
	else
	{
		UE_LOG(LogDamage, Warning, TEXT("ECollisionChannel::ECC_MAX is not valid! No falloff is added to damage"));
	}

	// didn't hit anything, assume nothing blocking the damage and victim is consequently visible
	// but since we don't have a hit result to pass back, construct a simple one, modeling the damage as having hit a point at the component's center.
	FVector const FakeHitLoc = VictimComp->GetComponentLocation();
	FVector const FakeHitNorm = (Origin - FakeHitLoc).GetSafeNormal();		// normal points back toward the epicenter
	OutHitResult = FHitResult(VictimComp->GetOwner(), VictimComp, FakeHitLoc, FakeHitNorm);
	return true;
}

bool ATDSExplosion::ApplyExplosionDamage()
{
	const UObject* WorldContextObject = GetWorld();
	float BaseDamage = ExplosionInfo.MaxDamage;
	float MinimumDamage = 0;
	const FVector& Origin = GetActorLocation();
	float DamageInnerRadius = ExplosionInfo.MaxDamageRange;
	float DamageOuterRadius = ExplosionInfo.DamageRange;
	float DamageFalloff = 1.f;
	TSubclassOf<class UDamageType> DamageTypeClass = UDamageType::StaticClass();
	const TArray<AActor*>& IgnoreActors = TArray<AActor*>();
	AActor* DamageCauser = this;
	AController* InstigatedByController = nullptr;
	ECollisionChannel DamagePreventionChannel = ECC_Visibility;

	FCollisionQueryParams SphereParams(SCENE_QUERY_STAT(ApplyRadialDamage), false, DamageCauser);

	SphereParams.AddIgnoredActors(IgnoreActors);

	// query scene to see what we hit
	TArray<FOverlapResult> Overlaps;
	if (UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull))
	{
		World->OverlapMultiByObjectType(Overlaps, Origin, FQuat::Identity, FCollisionObjectQueryParams(FCollisionObjectQueryParams::InitType::AllDynamicObjects), FCollisionShape::MakeSphere(DamageOuterRadius), SphereParams);
	}

	// collate into per-actor list of hit components
	TMap<AActor*, TArray<FHitResult> > OverlapComponentMap;
	for (int32 Idx = 0; Idx < Overlaps.Num(); ++Idx)
	{
		FOverlapResult const& Overlap = Overlaps[Idx];
		AActor* const OverlapActor = Overlap.GetActor();

		if (OverlapActor &&
			OverlapActor->CanBeDamaged() &&
			OverlapActor != DamageCauser &&
			Overlap.Component.IsValid())
		{
			FHitResult Hit;
			if (ComponentIsDamageableFrom(Overlap.Component.Get(), Origin, DamageCauser, IgnoreActors, DamagePreventionChannel, Hit))
			{
				TArray<FHitResult>& HitList = OverlapComponentMap.FindOrAdd(OverlapActor);
				HitList.Add(Hit);
			}
		}
	}

	bool bAppliedDamage = false;

	if (OverlapComponentMap.Num() > 0)
	{
		// make sure we have a good damage type
		TSubclassOf<UDamageType> const ValidDamageTypeClass = DamageTypeClass ? DamageTypeClass : TSubclassOf<UDamageType>(UDamageType::StaticClass());

		FRadialDamageEvent DmgEvent;
		DmgEvent.DamageTypeClass = ValidDamageTypeClass;
		DmgEvent.Origin = Origin;
		DmgEvent.Params = FRadialDamageParams(BaseDamage, MinimumDamage, DamageInnerRadius, DamageOuterRadius, DamageFalloff);

		// call damage function on each affected actors
		for (TMap<AActor*, TArray<FHitResult> >::TIterator It(OverlapComponentMap); It; ++It)
		{
			AActor* const Victim = It.Key();
			TArray<FHitResult> const& ComponentHits = It.Value();
			DmgEvent.ComponentHits = ComponentHits;

			Victim->TakeDamage(BaseDamage, DmgEvent, InstigatedByController, DamageCauser);
			for (auto effect : ExplosionInfo.StatusEffectList)
			{
				UTDSStatusEffect::CreateStatusEffect(Victim, effect);
			}

			bAppliedDamage = true;
		}
	}

	return bAppliedDamage;
}

void ATDSExplosion::SpawnCosmetics_NetMulticast_Implementation(UParticleSystem* ExplosionParticle, USoundBase* Sound)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),
		ExplosionParticle,
		FTransform(GetActorLocation()), true,
		EPSCPoolMethod::AutoRelease, true);

	UGameplayStatics::PlaySoundAtLocation(GetWorld(),
		Sound, GetActorLocation());
}

ATDSExplosion::ATDSExplosion()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(FName("Root"));
}

void ATDSExplosion::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		SpawnCosmetics_NetMulticast(ExplosionInfo.ExplosionParticle, ExplosionInfo.Sound);

		if (IsDebug)
		{
			DrawDebugSphere(GetWorld(), GetActorLocation(), ExplosionInfo.MaxDamageRange, 32, FColor::Red, false, 3);
			DrawDebugSphere(GetWorld(), GetActorLocation(), (ExplosionInfo.MaxDamageRange + ExplosionInfo.DamageRange) / 2, 32, FColor::Orange, false, 3);
			DrawDebugSphere(GetWorld(), GetActorLocation(), ExplosionInfo.DamageRange, 32, FColor::Yellow, false, 3);
		}

		ApplyExplosionDamage();

		SetLifeSpan(1.f);
	}
}

void ATDSExplosion::InitExplosion(const FExplosionInfo& ExpInf, const FHitResult &HR)
{
	ExplosionInfo = ExpInf;
	HitRes = HR;
}

