// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSWeapon.h"

#include "Kismet/KismetSystemLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "DrawDebugHelpers.h"
#include "Niagara/Public/NiagaraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Perception/AISense_Hearing.h"
#include "Net/UnrealNetwork.h"
#include "TDSProjectile.h"
#include "../../Effects/TDSStatusEffect.h"

// Sets default values
ATDSWeapon::ATDSWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetGenerateOverlapEvents(false);
	Mesh->SetCollisionProfileName(TEXT("NoCollision"));
	Mesh->SetupAttachment(Root);
	Mesh->SetVisibility(false);

	ShootLoc = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLoc"));
	ShootLoc->SetupAttachment(Root);
}

void ATDSWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATDSWeapon, State);
	DOREPLIFETIME(ATDSWeapon, Settings);
}

void ATDSWeapon::BeginPlay()
{
	Super::BeginPlay();
}

void ATDSWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATDSWeapon::SetFiring_Server_Implementation(bool nIsFiring)
{
	AuthSetFiring(nIsFiring);
}

bool ATDSWeapon::SetFiring_Server_Validate(bool nIsFiring)
{
	return true;
}

void ATDSWeapon::AuthSetFiring(bool nIsFiring)
{
	State.IsFiring = nIsFiring;
	if (State.IsFiring && !State.IsReloading)
	{
		float time_remain = 1.f / Settings.RateOfFire - (GetWorld()->TimeSeconds - State.TimeOfLastShot);
		float init_delta_time = (time_remain <= 0.f ? 0.f : time_remain);

		GetWorld()->GetTimerManager().SetTimer(
			FiringTimerHandle,
			this,
			&ATDSWeapon::AuthFire,
			1.f / Settings.RateOfFire,
			true,
			init_delta_time);
	}
	else
	{
		GetWorld()->GetTimerManager().ClearTimer(FiringTimerHandle);
	}
}

void ATDSWeapon::AuthFire()
{
	FVector loc = ShootLoc->GetComponentLocation();
	FRotator rot = GetOwner()->GetActorRotation();

	if (State.CurRounds <= 0)
	{
		//ReloadStart();
		OutOfAmmo_Auth.Broadcast();
		AuthSetFiring(false);
		return;
	}

	if (Settings.Type == EWeaponType::ProjectileType)
	{
		ATDSProjectile* proj = GetWorld()->SpawnActorDeferred<ATDSProjectile>(Settings.AmmoInfoList[State.CurAmmoTypeIndex].ProjectileInfo.ProjectileClass.Get(),
			FTransform(rot, loc), (AActor*)this, (APawn*)nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);

		if (IsValid(proj))
		{
			proj->InitProjectile(Settings.AmmoInfoList[State.CurAmmoTypeIndex].ProjectileInfo);
			proj->FinishSpawning(FTransform(rot, loc));
		}
	}
	else if (Settings.Type == EWeaponType::HitScanType)
	{
		TArray<FVector> EndPoints, Normals;

		for (int i = 0; i < Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.HitNum; i++)
		{
			FHitResult fire_hr;
			FVector fire_start = loc;
			FVector fire_dir = (rot + FRotator(Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.Dispersion * (2 * FMath::FRand() - 1),
				Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.Dispersion * (2 * FMath::FRand() - 1),
				Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.Dispersion * (2 * FMath::FRand() - 1))).Vector();
			FVector fire_end = fire_start + fire_dir * Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.Range;
			FCollisionQueryParams params;

			GetWorld()->LineTraceSingleByChannel(
				fire_hr,
				fire_start,
				fire_end,
				ECC_GameTraceChannel3
			);

			/*UKismetSystemLibrary::LineTraceSingle(
				GetWorld(),
				fire_start,
				fire_end,
				ECC_GameTraceChannel3,
				false,
				TArray<AActor*>(),
				EDrawDebugTrace::ForOneFrame,
				fire_hr,
				true
			);*/

			/*DrawDebugLine(GetWorld(),
				fire_start,
				fire_hr.bBlockingHit ? fire_hr.ImpactPoint : fire_end,
				FColor::Green,
				false,
				2);*/

			/*UNiagaraComponent* nc = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),
				Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.Trail,
				GetActorLocation(),
				FRotator::ZeroRotator,
				FVector(1.f),
				true,
				false,
				ENCPoolMethod::AutoRelease,
				true);

			nc->SetVectorParameter(FName("EndPoint"), 
				fire_hr.bBlockingHit ? fire_hr.ImpactPoint : fire_end);


			nc->Activate();	*/

			EndPoints.Add(fire_hr.bBlockingHit ? fire_hr.ImpactPoint : fire_end);

			if (fire_hr.bBlockingHit)
			{
				float damage = FMath::Lerp(Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.MaxDamage,
					Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.MinDamage,
					fire_hr.Time);

				fire_hr.Actor->TakeDamage(damage, FPointDamageEvent(), nullptr, this);
				for (auto effect : Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.StatusEffectList)
				{
					UTDSStatusEffect::CreateStatusEffect(fire_hr.Actor.Get(), effect);
				}

				Normals.Add(fire_hr.ImpactNormal);
			}
			else
			{
				Normals.Add(FVector::ZeroVector);
			}
		}

		DrawHitscan_NetMulticast(EndPoints, Normals);
	}

	Fire_Cosmetics_NetMulticast();

	UAISense_Hearing::ReportNoiseEvent(GetWorld(), loc, 1.f, GetOwner());
	State.CurRounds--;
	OnCurRoundsChanged.Broadcast(this);
	State.TimeOfLastShot = GetWorld()->TimeSeconds;
}

void ATDSWeapon::Fire_Cosmetics_NetMulticast_Implementation()
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(),
		Settings.FireSound, ShootLoc->GetComponentLocation());
	SpawnRound();
	OnWeaponFireStart.Broadcast(this);
}

void ATDSWeapon::DrawHitscan_NetMulticast_Implementation(const TArray<FVector>& EndPoints, const TArray<FVector>& Normals)
{
	for (int i = 0; i < EndPoints.Num(); i++)
	{
		UNiagaraComponent* nc = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),
			Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.Trail,
			GetActorLocation(),
			FRotator::ZeroRotator,
			FVector(1.f),
			true,
			false,
			ENCPoolMethod::AutoRelease,
			true);

		nc->SetVectorParameter(FName("EndPoint"), EndPoints[i]);

		nc->Activate();

		if (Normals[i].SizeSquared() > 0.5)
		{
			FTransform particle_trans;
			particle_trans.SetLocation(EndPoints[i]);
			particle_trans.SetRotation(
				UKismetMathLibrary::MakeRotFromZ(Normals[i]).Quaternion());

			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.HitParticle,
				particle_trans, true, EPSCPoolMethod::AutoRelease, true);
		}
	}
}

void ATDSWeapon::OnRep_Settings()
{
	State.CurRounds = Settings.MaxRounds;
	OnCurRoundsChanged.Broadcast(this);
}

void ATDSWeapon::OnRep_State(const FDynamicWeaponInfo& oldState)
{
	if (State.CurRounds != oldState.CurRounds)
	{
		OnCurRoundsChanged.Broadcast(this);
	}
}

void ATDSWeapon::InitWeapon(const FWeaponInfo& nSettings)
{
	Settings = nSettings;

	State.CurRounds = nSettings.MaxRounds;
	OnCurRoundsChanged.Broadcast(this);
}

int ATDSWeapon::GetCurrentRounds() const
{
	return State.CurRounds;
}

int ATDSWeapon::GetMaxRounds() const
{
	return Settings.MaxRounds;
}

void ATDSWeapon::AuthReloadStart(int* newAmmoCache)
{
	if (!State.IsReloading)
	{
		State.IsReloading = true;
		AuthSetFiring(false);
		AmmoCache = newAmmoCache;

		GetWorld()->GetTimerManager().SetTimer(ReloadTimerHandle,
			this,
			&ATDSWeapon::AuthReloadEnd,
			Settings.ReloadTime);
		
		ReloadStart_NetMulticast();
	}
}

void ATDSWeapon::ReloadStart_NetMulticast_Implementation()
{
	OnWeaponReloadStart.Broadcast(this);
}

void ATDSWeapon::AuthReloadEnd()
{
	State.IsReloading = false;

	if (AmmoCache != nullptr)
	{
		int delta = FMath::Min(Settings.MaxRounds - State.CurRounds, *AmmoCache);

		State.CurRounds += delta;
		*AmmoCache -= delta;
		AmmoCache = nullptr;
		OnCurRoundsChanged.Broadcast(this);
	}
}

UStaticMesh* ATDSWeapon::GetMagazine_Implementation() const
{
	return nullptr;
}
