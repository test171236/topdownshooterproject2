// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "../../FuncLib/TDSTypes.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "TDSProjectile.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDSProjectile();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USphereComponent* RootSphere = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UProjectileMovementComponent* ProjectileMovement = nullptr;
	UPROPERTY(ReplicatedUsing = OnRep_Settings)
	FProjectileInfo Settings;
	UFUNCTION()
	void OnRep_Settings();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	UNiagaraComponent* TrailComp = nullptr;

	float CurrentDistance = 0.f;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION(NetMulticast, Unreliable)
	void BulletHit_NetMulticast(FVector_NetQuantize ImpactPoint, FVector_NetQuantizeNormal ImpactNormal);
	void BulletHit_NetMulticast_Implementation(FVector_NetQuantize ImpactPoint, FVector_NetQuantizeNormal ImpactNormal);
	UFUNCTION()
	void BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void InitProjectile(const FProjectileInfo& info);
	UFUNCTION(NetMulticast, reliable)
	void InitProjectile_NetMulticast(const FProjectileInfo& info);
	void InitProjectile_NetMulticast_Implementation(const FProjectileInfo& info);

};
