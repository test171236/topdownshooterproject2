// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSProjectile.h"

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"
#include "NiagaraComponent.h"

#include "TDSExplosion.h"
#include "../../Effects/TDSStatusEffect.h"

// Sets default values
ATDSProjectile::ATDSProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;

	RootSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	RootComponent = RootSphere;
	RootSphere->OnComponentHit.AddDynamic(this, &ATDSProjectile::BulletCollisionSphereHit);
	RootSphere->OnComponentBeginOverlap.AddDynamic(this, &ATDSProjectile::BulletCollisionSphereBeginOverlap);
	RootSphere->OnComponentEndOverlap.AddDynamic(this, &ATDSProjectile::BulletCollisionSphereEndOverlap);
	RootSphere->bReturnMaterialOnMove = true;
	RootSphere->SetCanEverAffectNavigation(false);
	RootSphere->SetCollisionProfileName(TEXT("Projectile"));

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetCanEverAffectNavigation(false);
	Mesh->SetCollisionProfileName(TEXT("NoCollision"));

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	ProjectileMovement->UpdatedComponent = RootComponent;
	ProjectileMovement->InitialSpeed = 1.f;
	ProjectileMovement->MaxSpeed = 0.f;

	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;
}

void ATDSProjectile::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATDSProjectile, Settings);
}

// Called when the game starts or when spawned
void ATDSProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDSProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CurrentDistance += DeltaTime * Settings.Speed;
}

void ATDSProjectile::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (HasAuthority())
	{
		float damage = 0.f;

		if (CurrentDistance <= Settings.MaxDamageRange)
		{
			damage = Settings.MaxDamage;
		}
		else if (CurrentDistance >= Settings.MinDamageRange)
		{
			damage = Settings.MinDamage;
		}
		else
		{
			damage = UKismetMathLibrary::Lerp(Settings.MaxDamage,
				Settings.MinDamage,
				(CurrentDistance - Settings.MaxDamageRange) / (Settings.MinDamageRange - Settings.MaxDamageRange));
		}

		if (damage > 0)
		{
			OtherActor->TakeDamage(damage, FPointDamageEvent(), nullptr, this);
			for (auto effect : Settings.StatusEffectList)
				UTDSStatusEffect::CreateStatusEffect(OtherActor, effect);
		}

		BulletHit_NetMulticast(Hit.ImpactPoint, Hit.ImpactNormal);

		if (Settings.IsExplosive)
		{
			ATDSExplosion* exp = GetWorld()->SpawnActorDeferred<ATDSExplosion>(
				ATDSExplosion::StaticClass(),
				FTransform(GetActorLocation()),
				GetOwner());
			exp->InitExplosion(Settings.ExplosionInfo, Hit);
			exp->FinishSpawning(FTransform(GetActorLocation()));
		}
		//Destroy();
		SetLifeSpan(1.f);
	}
}

void ATDSProjectile::BulletHit_NetMulticast_Implementation(FVector_NetQuantize ImpactPoint, FVector_NetQuantizeNormal ImpactNormal)
{
	FTransform particle_trans;
	particle_trans.SetLocation(ImpactPoint);
	particle_trans.SetRotation(
		UKismetMathLibrary::MakeRotFromZ(ImpactNormal).Quaternion());

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Settings.HitParticle,
		particle_trans, true, EPSCPoolMethod::AutoRelease, true);

	RootSphere->SetCollisionProfileName(TEXT("NoCollision"));
	SetActorHiddenInGame(true);

	ProjectileMovement->SetVelocityInLocalSpace(FVector::ZeroVector);
}

void ATDSProjectile::BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void ATDSProjectile::BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void ATDSProjectile::OnRep_Settings()
{
	SetLifeSpan(Settings.LifeTime);
	ProjectileMovement->SetVelocityInLocalSpace(FVector(Settings.Speed,0,0));

	TrailComp = UNiagaraFunctionLibrary::SpawnSystemAttached(Settings.Trail, Mesh, NAME_None,
		FVector(0.f, 0.f, 0.f), FRotator(0.f), EAttachLocation::Type::KeepRelativeOffset,
		true, true, ENCPoolMethod::AutoRelease, true);
}

void ATDSProjectile::InitProjectile_NetMulticast_Implementation(const FProjectileInfo& info)
{
	InitProjectile(info);
}

void ATDSProjectile::InitProjectile(const FProjectileInfo& info)
{
	Settings = info;

	SetLifeSpan(Settings.LifeTime);
	ProjectileMovement->InitialSpeed = Settings.Speed;

	TrailComp = UNiagaraFunctionLibrary::SpawnSystemAttached(Settings.Trail, Mesh, NAME_None,
		FVector(0.f, 0.f, 0.f), FRotator(0.f), EAttachLocation::Type::KeepRelativeOffset, 
		true, true, ENCPoolMethod::AutoRelease, true);
}

