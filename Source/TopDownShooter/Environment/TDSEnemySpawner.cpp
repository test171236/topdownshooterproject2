// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSEnemySpawner.h"
#include "Components/CapsuleComponent.h"
#include "../Game/TopDownShooterGameMode.h"
#include "../Enemies/TDSEnemy.h"

ATDSEnemySpawner::ATDSEnemySpawner()
{
	PrimaryActorTick.bCanEverTick = false;

}

bool ATDSEnemySpawner::SpawnEnemy(TSubclassOf<ATDSEnemy> EnemyClass) const
{
	bool res = false;
	float height = EnemyClass.GetDefaultObject()->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	FHitResult hr;

	GetWorld()->LineTraceSingleByChannel(
		hr,
		GetActorLocation(),
		GetActorLocation() + FVector(0,0,-1e3),
		ECC_GameTraceChannel1
	);

	if (hr.bBlockingHit)
	{
		FActorSpawnParameters asp;
		asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

		res = IsValid(GetWorld()->SpawnActor<ATDSEnemy>(EnemyClass, 
			FTransform(hr.ImpactPoint + FVector(0, 0, height)), asp));
	}

	return res;
}

void ATDSEnemySpawner::BeginPlay()
{
	Super::BeginPlay();

	ATopDownShooterGameMode* gm = Cast<ATopDownShooterGameMode>(GetWorld()->GetAuthGameMode());

	if (gm)
	{
		gm->AddEnemySpawner(this);
	}
}


