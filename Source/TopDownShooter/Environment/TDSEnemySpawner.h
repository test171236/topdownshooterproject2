// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSEnemySpawner.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	ATDSEnemySpawner();

	bool SpawnEnemy(TSubclassOf<class ATDSEnemy> EnemyClass) const;
protected:
	virtual void BeginPlay() override;
};
