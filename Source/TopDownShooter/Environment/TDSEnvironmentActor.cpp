// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSEnvironmentActor.h"

#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"

#include "../Effects/TDSStatusEffect.h"

ATDSEnvironmentActor::ATDSEnvironmentActor()
{
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;
}

void ATDSEnvironmentActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Here we list the variables we want to replicate
	DOREPLIFETIME(ATDSEnvironmentActor, EffectsList);
}

bool ATDSEnvironmentActor::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (auto effect : EffectsList)
	{
		if (effect)
		{
			WroteSomething |= Channel->ReplicateSubobject(effect, *Bunch, *RepFlags);
		}
	}
	return WroteSomething;
}


