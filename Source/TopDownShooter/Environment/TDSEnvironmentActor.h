// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Effects/TDSAffectedActor.h"
#include "TDSEnvironmentActor.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSEnvironmentActor : public AActor, public ITDSAffectedActor
{
	GENERATED_BODY()
	
	UPROPERTY(ReplicatedUsing = OnRep_EffectsList)
	TArray<class UTDSStatusEffect*> EffectsList;
	UFUNCTION()
	void OnRep_EffectsList(const TArray<class UTDSStatusEffect*>& oldEffectsList) override { ITDSAffectedActor::OnRep_EffectsList(oldEffectsList); }

public:	
	ATDSEnvironmentActor();

protected:
	bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

public:	
	TArray<class UTDSStatusEffect*>& GetEffectsList() override { return EffectsList; }
};
