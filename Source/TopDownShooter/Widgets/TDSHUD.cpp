// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSHUD.h"

#include "Kismet/GameplayStatics.h"
#include "TDSHealthWidget.h"
#include "TDS_StaminaWidget.h"
#include "TDSInventoryWidget.h"
#include "TDSInteractableItemWidget.h"
#include "../Character/TopDownShooterCharacter.h"
#include "../Character/TDSStaminaSystem.h"

void ATDSHUD::BeginPlay(void)
{
    Super::BeginPlay();

    FTimerHandle timer;
    GetWorld()->GetTimerManager().SetTimer(timer, this, &ATDSHUD::InitWidgets, 1.f);
}

void ATDSHUD::InitWidgets()
{
    HealthWidget = Cast<UTDSHealthWidget>(CreateWidget(GetWorld(), HealthWidgetClass, TEXT("HealthWidget")));
    HealthWidget->AddToViewport();
    StaminaWidget = Cast<UTDS_StaminaWidget>(CreateWidget(GetWorld(), StaminaWidgetClass, TEXT("StaminaWidget")));
    StaminaWidget->AddToViewport();
    InventoryWidget = Cast<UTDSInventoryWidget>(CreateWidget(GetWorld(), InventoryWidgetClass, TEXT("InventoryWidget")));
    InventoryWidget->AddToViewport();
    InteractableItemWidget = Cast<UTDSInteractableItemWidget>(CreateWidget(GetWorld(), InteractableItemWidgetClass, TEXT("InteractableItemWidget")));
    InteractableItemWidget->AddToViewport();
}
