// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_StaminaWidget.h"

#include "UMG/Public/Components/ProgressBar.h"
#include "Kismet/GameplayStatics.h"
#include "../Character/TopDownShooterCharacter.h"
#include "../Character/TDSStaminaSystem.h"

void UTDS_StaminaWidget::NativeConstruct()
{
    Super::NativeConstruct();

    ATopDownShooterCharacter* ch = Cast<ATopDownShooterCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
    if (IsValid(ch))
    {
        ch->StaminaSystem->OnStaminaUpdate.AddDynamic(this, &UTDS_StaminaWidget::Update);
        Update(ch->StaminaSystem->GetCurrentStaminaPercent());
    }
}

void UTDS_StaminaWidget::Update(float Percent)
{
    StaminaBar->SetPercent(Percent);
}
