// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDSBossHealthWidget.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSBossHealthWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UProgressBar* HealthBar;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* BossName;
public:
	virtual void NativeConstruct() override;
	void Init(class UTDSHealthSystem* HealthSystem);

	UFUNCTION()
	void Update(class UTDSHealthSystem* HealthSystem);

};
