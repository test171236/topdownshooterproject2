// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDSInventoryWidget.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSInventoryWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UImage* Icon1;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UImage* Icon2;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UImage* Icon3;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UImage* Icon4;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* AmmoText1;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* AmmoText2;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* AmmoText3;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* AmmoText4;

	const int WEAPON_SLOT_NUM = 4;

	UPROPERTY(BlueprintReadWrite)
	TArray<class UImage*> IconList;
	UPROPERTY(BlueprintReadWrite)
	TArray<class UTextBlock*> AmmoTextList;
	UPROPERTY(BlueprintReadWrite)
	TArray<class UMaterialInstanceDynamic*> IconMaterialList;

	UPROPERTY(EditDefaultsOnly)
	FLinearColor ActiveColor = FLinearColor::Red;
	UPROPERTY(EditDefaultsOnly)
	FLinearColor DefaultColor = FLinearColor::Black;

	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* WeaponIconMaterial;

public:
	virtual void NativeConstruct() override;
	UFUNCTION()
	void BindDelegates(ATopDownShooterCharacter* Character, ATopDownShooterPlayerController* NewController);

	UFUNCTION()
	void UpdateCurrentWeaponIndex(int Ind);
	UFUNCTION()
	void UpdateWeaponIcon(int Ind, const FSoftObjectPath& Path);
	UFUNCTION()
	void UpdateAmmoSlots(class UTDSInventorySystem* Inventory);
};
