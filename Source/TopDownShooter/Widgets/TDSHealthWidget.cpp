// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSHealthWidget.h"

#include "UMG/Public/Components/ProgressBar.h"
#include "Kismet/GameplayStatics.h"
#include "../Character/TopDownShooterCharacter.h"
#include "../Character/TDSHealthSystem.h"

void UTDSHealthWidget::NativeConstruct()
{
    Super::NativeConstruct();

    ATopDownShooterCharacter* ch = Cast<ATopDownShooterCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
    if (IsValid(ch))
    {
        ch->HealthSystem->OnHealthChange.AddDynamic(this, &UTDSHealthWidget::Update);
        Update(ch->HealthSystem);
    }
}

void UTDSHealthWidget::Update(UTDSHealthSystem* HealthSystem)
{
    HealthBar->SetPercent(HealthSystem->GetCurHP() / HealthSystem->GetMaxHP());
    ShieldBar->SetPercent(HealthSystem->GetCurShield() / HealthSystem->GetMaxShield());
}

