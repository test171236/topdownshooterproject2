// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "TDSHUD.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API ATDSHUD : public AHUD
{
	GENERATED_BODY()
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UTDSHealthWidget> HealthWidgetClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UTDS_StaminaWidget> StaminaWidgetClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UTDSInventoryWidget> InventoryWidgetClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UTDSInteractableItemWidget> InteractableItemWidgetClass;

	UFUNCTION()
	void InitWidgets();

protected:
	UPROPERTY(BlueprintReadWrite)
	class UTDSHealthWidget* HealthWidget;
	UPROPERTY(BlueprintReadWrite)
	class UTDS_StaminaWidget* StaminaWidget;
	UPROPERTY(BlueprintReadWrite)
	class UTDSInventoryWidget* InventoryWidget;
	UPROPERTY(BlueprintReadWrite)
	class UTDSInteractableItemWidget* InteractableItemWidget;

	UFUNCTION(BlueprintCallable)
	virtual void BeginPlay() override;
	
public:

};
