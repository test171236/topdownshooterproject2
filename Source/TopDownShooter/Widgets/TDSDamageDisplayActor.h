// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSDamageDisplayActor.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSDamageDisplayActor : public AActor
{
	GENERATED_BODY()
	
	FVector Dir = FVector(0,1,0);

	UPROPERTY(EditAnywhere)
	float Speed = 500.f;

	UPROPERTY(EditAnywhere)
	struct FLinearColor LoColor;
	UPROPERTY(EditAnywhere)
	struct FLinearColor HiColor;
	UPROPERTY(EditAnywhere)
	struct FLinearColor ShieldColor;

public:	
	ATDSDamageDisplayActor();

protected:
	UPROPERTY(BlueprintReadWrite)
	class UWidgetComponent* DamageWidgetComponent;

	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	void ChangeText(float Damage, bool isShield, float HPPercent);
};
