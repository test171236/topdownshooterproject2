// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSInventoryWidget.h"

#include "UMG/Public/Components/Border.h"
#include "UMG/Public/Components/Image.h"
#include "UMG/Public/Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"
#include "../Character/TopDownShooterCharacter.h"
#include "../Character/TDSInventorySystem.h"
#include "../Game/TopDownShooterPlayerController.h"

void UTDSInventoryWidget::NativeConstruct()
{
    Super::NativeConstruct();

    IconList.Empty();
    IconList.Add(Icon1);
    IconList.Add(Icon2);
    IconList.Add(Icon3);
    IconList.Add(Icon4);

    AmmoTextList.Empty();
    AmmoTextList.Add(AmmoText1);
    AmmoTextList.Add(AmmoText2);
    AmmoTextList.Add(AmmoText3);
    AmmoTextList.Add(AmmoText4);

    IconMaterialList.Empty();
    for (auto& icon : IconList)
    {
        UMaterialInstanceDynamic* mid = UMaterialInstanceDynamic::Create(WeaponIconMaterial, this);
        icon->SetBrushFromMaterial(mid);
        IconMaterialList.Add(mid);
    }

    ATopDownShooterPlayerController* cont = Cast<ATopDownShooterPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
    if (cont)
    {
        ATopDownShooterCharacter* Character = Cast<ATopDownShooterCharacter>(cont->GetPawn());
        if (Character)
        {
            Character->InventorySystem->OnCurrentWeaponIndexUpdate.AddDynamic(this, &UTDSInventoryWidget::UpdateCurrentWeaponIndex);
            UpdateCurrentWeaponIndex(Character->InventorySystem->GetCurrentWeaponIndex());

            Character->InventorySystem->OnUpdateWeaponSlot.AddDynamic(this, &UTDSInventoryWidget::UpdateWeaponIcon);
            for (int i = 0; i < WEAPON_SLOT_NUM; i++)
            {
                if (IsValid(Character->InventorySystem->WeaponList[i]))
                {
                    UpdateWeaponIcon(i, Character->InventorySystem->WeaponList[i]->Settings.Icon);
                }
                else if (IsValid(Character->InventorySystem->NoWeapon))
                {
                    UpdateWeaponIcon(i, Character->InventorySystem->NoWeapon->Settings.Icon);
                }
            }
            Character->InventorySystem->OnUpdateAmmoSlots.AddDynamic(this, &UTDSInventoryWidget::UpdateAmmoSlots);
            UpdateAmmoSlots(Character->InventorySystem);
        }
        else
        {
            //cont->OnPossesedBy.AddDynamic(this, &UTDSInventoryWidget::BindDelegates);
        }
    }
}

void UTDSInventoryWidget::BindDelegates(ATopDownShooterCharacter* Character, ATopDownShooterPlayerController* NewController)
{
    if (IsValid(Character))
    {
        Character->InventorySystem->OnCurrentWeaponIndexUpdate.AddDynamic(this, &UTDSInventoryWidget::UpdateCurrentWeaponIndex);
        UpdateCurrentWeaponIndex(Character->InventorySystem->GetCurrentWeaponIndex());

        Character->InventorySystem->OnUpdateWeaponSlot.AddDynamic(this, &UTDSInventoryWidget::UpdateWeaponIcon);
        for (int i = 0; i < WEAPON_SLOT_NUM; i++)
        {
            if (IsValid(Character->InventorySystem->WeaponList[i]))
            {
                UpdateWeaponIcon(i, Character->InventorySystem->WeaponList[i]->Settings.Icon);
            }
            else if (IsValid(Character->InventorySystem->NoWeapon))
            {
                UpdateWeaponIcon(i, Character->InventorySystem->NoWeapon->Settings.Icon);
            }
        }
        Character->InventorySystem->OnUpdateAmmoSlots.AddDynamic(this, &UTDSInventoryWidget::UpdateAmmoSlots);
        UpdateAmmoSlots(Character->InventorySystem);
    }
}

void UTDSInventoryWidget::UpdateCurrentWeaponIndex(int Ind)
{
    if (Ind < 0 || Ind >= WEAPON_SLOT_NUM)
    {
        return;
    }

    for (int i = 0; i < IconMaterialList.Num(); i++)
    {
        if (i == Ind)
        {
            IconMaterialList[i]->SetVectorParameterValue(TEXT("BackgroundColor"), ActiveColor);
        }
        else
        {
            IconMaterialList[i]->SetVectorParameterValue(TEXT("BackgroundColor"), DefaultColor);
        }
    }
}

void UTDSInventoryWidget::UpdateWeaponIcon(int Ind, const FSoftObjectPath& Path)
{
    TScriptInterface<ISlateTextureAtlasInterface> Icon(Path.TryLoad());
    UMaterialInstanceDynamic* mid = IconMaterialList[Ind];
    FLinearColor lc;

    if (Icon)
    {
        lc.R = Icon->GetSlateAtlasData().StartUV.X;
        lc.G = Icon->GetSlateAtlasData().StartUV.Y;
        lc.B = Icon->GetSlateAtlasData().SizeUV.X;
        lc.A = Icon->GetSlateAtlasData().SizeUV.Y;
    }
    else
    {
        lc.R = 0;
        lc.G = 0;
        lc.B = 0.01;
        lc.A = 0.01;
    }

    mid->SetVectorParameterValue(TEXT("Start_SizeUV"), lc);
}

void UTDSInventoryWidget::UpdateAmmoSlots(UTDSInventorySystem* Inventory)
{
    if (!Inventory->NoWeapon)
    {
        return;
    }

    for (int i = 0; i < WEAPON_SLOT_NUM; i++)
    {
        ATDSWeapon* weapon = Inventory->WeaponList[i];
        if (!IsValid(weapon))
        {
            weapon = Inventory->NoWeapon;
        }

        if (weapon->State.CurAmmoTypeIndex >= weapon->Settings.AmmoInfoList.Num())
        {
            AmmoTextList[i]->SetVisibility(ESlateVisibility::Hidden);
        } else {
            FString str;
            str = str.Printf(TEXT("%d/%d"), weapon->GetCurrentRounds(),
                Inventory->AmmoAmount[(int)weapon->GetCurrentAmmoType()]);
            AmmoTextList[i]->SetText(FText::FromString(str));
            AmmoTextList[i]->SetVisibility(ESlateVisibility::Visible);
        }
    }
}
