// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDS_StaminaWidget.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDS_StaminaWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UProgressBar* StaminaBar;

public: 
	virtual void NativeConstruct() override;
	
	UFUNCTION()
	void Update(float Percent);
};
