// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSBossHealthWidget.h"
#include "UMG/Public/Components/ProgressBar.h"
#include "UMG/Public/Components/TextBlock.h"
#include "../Character/TDSHealthSystem.h"

void UTDSBossHealthWidget::NativeConstruct()
{
    Super::NativeConstruct();

    BossName->SetText(FText::FromString("Boss"));
}

void UTDSBossHealthWidget::Init(UTDSHealthSystem* HealthSystem)
{
    HealthSystem->OnHealthChange.AddDynamic(this, &UTDSBossHealthWidget::Update);
    Update(HealthSystem);
}

void UTDSBossHealthWidget::Update(UTDSHealthSystem* HealthSystem)
{
    HealthBar->SetPercent(HealthSystem->GetCurHP() / HealthSystem->GetMaxHP());
}

