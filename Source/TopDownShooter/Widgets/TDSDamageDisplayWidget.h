// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDSDamageDisplayWidget.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSDamageDisplayWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* Text;
};
