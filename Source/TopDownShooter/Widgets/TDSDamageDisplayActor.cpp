// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSDamageDisplayActor.h"
#include "UMG/Public/Components/TextBlock.h"
#include "Components/WidgetComponent.h"
#include "TDSDamageDisplayWidget.h"

// Sets default values
ATDSDamageDisplayActor::ATDSDamageDisplayActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	InitialLifeSpan = 2.f;

	DamageWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("DamageWidget");
	SetRootComponent(DamageWidgetComponent);
	DamageWidgetComponent->SetCollisionProfileName(TEXT("NoCollision"));
}

// Called when the game starts or when spawned
void ATDSDamageDisplayActor::BeginPlay()
{
	Super::BeginPlay();
	
	//DamageWidget = Cast<UTDSDamageDisplayWidget>(CreateWidget(GetWorld(), WidgetClass, TEXT("DamageWidget")));
	//DamageWidgetComponent->SetWidget(DamageWidget);

	Dir = FVector(0,0,1).RotateAngleAxis(30, FVector(0, 1, 0)).RotateAngleAxis(FMath::FRandRange(-30, 30), FVector(1, 0, 0));
}

// Called every frame
void ATDSDamageDisplayActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorWorldOffset(Dir * Speed * DeltaTime);
}

void ATDSDamageDisplayActor::ChangeText(float Damage, bool isShield, float HPPercent)
{
	UTDSDamageDisplayWidget* DamageWidget = Cast<UTDSDamageDisplayWidget>(DamageWidgetComponent->GetUserWidgetObject());

	FNumberFormattingOptions nfo;
	nfo.MaximumFractionalDigits = 2;
	nfo.MinimumFractionalDigits = 2;
	DamageWidget->Text->SetText(FText::AsNumber(Damage, &nfo));
	FSlateColor color;
	if (isShield)
	{
		color = ShieldColor;
	}
	else
	{
		color = FLinearColor::LerpUsingHSV(LoColor, HiColor, HPPercent);
	}
	DamageWidget->Text->SetColorAndOpacity(color);
}

