// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSDropComponent.h"

UTDSDropComponent::UTDSDropComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

AActor* UTDSDropComponent::DropItem()
{
	AActor* res = nullptr;

	float prob = FMath::FRand();
    TSubclassOf<AActor> ItemClass = AActor::StaticClass();
    float total_prob = 0;

    for (int i = 0; i < DropSettings.Num(); i++)
    {
        total_prob += DropSettings[i].Prob;
        if (prob <= total_prob)
        {
            ItemClass = DropSettings[i].DropItemClass;
            break;
        }
    }
    
    AActor* def_obj = Cast<AActor>(ItemClass->GetDefaultObject());
    if (ItemClass != AActor::StaticClass() && def_obj)
    {
        if (def_obj->GetIsReplicated())
        {
            FActorSpawnParameters asp;
            asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
            res = GetWorld()->SpawnActor<AActor>(ItemClass, FTransform(GetOwner()->GetActorLocation()), asp);
        }
        else
        {
            DropNotReplicatedItem_NetMulticast(ItemClass);
        }
    }

	return res;
}


void UTDSDropComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UTDSDropComponent::DropNotReplicatedItem_NetMulticast_Implementation(TSubclassOf<AActor> ItemClass)
{
    FActorSpawnParameters asp;
    asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
    GetWorld()->SpawnActor<AActor>(ItemClass, FTransform(GetOwner()->GetActorLocation()), asp);
}

