// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSInteractable.h"
#include "../FuncLib/TDSTypes.h"
#include "TDSPickUpAmmo.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSPickUpAmmo : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDSPickUpAmmo();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	class USphereComponent* Sphere;
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	class UStaticMeshComponent* Mesh;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TMap<EAmmoType, UStaticMesh *> MeshList;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	EAmmoType AmmoType;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int Ammount;

public:
};
