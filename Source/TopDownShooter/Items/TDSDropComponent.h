// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSDropComponent.generated.h"

USTRUCT()
struct FDropSettings {
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere)
	float Prob;
	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> DropItemClass;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWNSHOOTER_API UTDSDropComponent : public UActorComponent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	TArray<FDropSettings> DropSettings;

public:	
	UTDSDropComponent();

	UFUNCTION(BlueprintCallable)
	AActor* DropItem();

protected:
	virtual void BeginPlay() override;

	UFUNCTION(NetMulticast, Reliable)
	void DropNotReplicatedItem_NetMulticast(TSubclassOf<AActor> ItemClass);
};
