// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSPickUpWeapon.h"

#include "Engine/DataTable.h"
#include "Components/SphereComponent.h"
#include "Net/UnrealNetwork.h"
#include "../Character/TopDownShooterCharacter.h"
#include "../Character/TDSInventorySystem.h"
#include "../Character/Weapon/TDSWeapon.h"
#include "../FuncLib/TDSTypes.h"

// Sets default values
ATDSPickUpWeapon::ATDSPickUpWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;

	Sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	SetRootComponent(Sphere);
	Sphere->SetCollisionProfileName(TEXT("OverlapAll"));
}

// Called when the game starts or when spawned
void ATDSPickUpWeapon::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		if (!WeaponInfoTable)
		{
			return;
		}

		FWeaponInfo* info = WeaponInfoTable->FindRow<FWeaponInfo>(StartWeaponName, "");

		if (info)
		{
			FActorSpawnParameters asp;
			asp.Owner = this;
			asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			ATDSWeapon* newWeapon = GetWorld()->SpawnActor<ATDSWeapon>(info->WeaponClass, asp);
			if (newWeapon)
			{
				newWeapon->InitWeapon(*info);
				InitWeapon(newWeapon);
			}
		}
	}
	else
	{
		if (IsValid(Weapon))
		{
			FAttachmentTransformRules rule(EAttachmentRule::KeepRelative, false);
			Weapon->AttachToActor(this, rule);
			Weapon->SetActorRelativeTransform(FTransform());
			Weapon->Mesh->SetVisibility(true);
			Weapon->SetOwner(this);
		}
	}
}

void ATDSPickUpWeapon::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if (HasAuthority())
	{
		if (!WeaponInfoTable)
		{
			return;
		}

		FWeaponInfo* info = WeaponInfoTable->FindRow<FWeaponInfo>(StartWeaponName, "");

		if (info)
		{
			FActorSpawnParameters asp;
			asp.Owner = this;
			asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			ATDSWeapon* newWeapon = GetWorld()->SpawnActor<ATDSWeapon>(info->WeaponClass, asp);
			if (newWeapon)
			{
				newWeapon->InitWeapon(*info);
				InitWeapon(newWeapon);
			}
		}
	}
	else
	{
		if (IsValid(Weapon))
		{
			FAttachmentTransformRules rule(EAttachmentRule::KeepRelative, false);
			Weapon->AttachToActor(this, rule);
			Weapon->SetActorRelativeTransform(FTransform());
			Weapon->Mesh->SetVisibility(true);
			Weapon->SetOwner(this);
		}
	}
}

void ATDSPickUpWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATDSPickUpWeapon, Weapon);
}

void ATDSPickUpWeapon::OnRep_Weapon(ATDSWeapon *oldWeapon)
{
	if (IsValid(Weapon))
	{
		FAttachmentTransformRules rule(EAttachmentRule::KeepRelative, false);
		Weapon->AttachToActor(this, rule);
		Weapon->SetActorRelativeTransform(FTransform());
		Weapon->Mesh->SetVisibility(true);
		Weapon->SetOwner(this);
	}
}

void ATDSPickUpWeapon::EndPlay(EEndPlayReason::Type Reason)
{
	if (HasAuthority() && IsValid(Weapon))
	{
		Weapon->Destroy();
	}

	Super::EndPlay(Reason);
}

void ATDSPickUpWeapon::BeginDestroy()
{
	if (HasAuthority() && IsValid(Weapon))
	{
		Weapon->Destroy();
	}

	Super::BeginDestroy();
}

void ATDSPickUpWeapon::Interact(ATopDownShooterCharacter* Player)
{
	if (Player->PickUpWeapon(Weapon))
	{
		Weapon = nullptr;
		Destroy();
	}
}

FString ATDSPickUpWeapon::GetHUDText() const
{
	return FString("Press E to take");
}

void ATDSPickUpWeapon::InitWeapon(ATDSWeapon* newWeapon)
{
	if (IsValid(Weapon))
	{
		Weapon->Destroy();
	}

	Weapon = newWeapon;

	if (IsValid(Weapon))
	{
		FAttachmentTransformRules rule(EAttachmentRule::KeepRelative, false);
		Weapon->AttachToActor(this, rule);
		Weapon->SetActorRelativeTransform(FTransform());
		Weapon->Mesh->SetVisibility(true);
		Weapon->SetOwner(this);
	}
}
