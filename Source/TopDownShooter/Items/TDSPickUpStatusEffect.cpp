// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSPickUpStatusEffect.h"
#include "Components/SphereComponent.h"
#include "../Effects/TDSStatusEffect.h"
#include "../Character/TopDownShooterCharacter.h"

// Sets default values
ATDSPickUpStatusEffect::ATDSPickUpStatusEffect()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;

	Sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	SetRootComponent(Sphere);
	Sphere->SetCollisionProfileName(TEXT("OverlapAll"));
}

// Called when the game starts or when spawned
void ATDSPickUpStatusEffect::BeginPlay()
{
	Super::BeginPlay();

}

void ATDSPickUpStatusEffect::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	ATopDownShooterCharacter* player = Cast<ATopDownShooterCharacter>(OtherActor);
	if (IsValid(player) && HasAuthority())
	{
		UTDSStatusEffect* effect = UTDSStatusEffect::CreateStatusEffect(OtherActor, EffectClass);
		if (effect)
		{
			Destroy();
		}
	}
}
