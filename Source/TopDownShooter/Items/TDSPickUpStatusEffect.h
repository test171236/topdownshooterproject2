// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSPickUpStatusEffect.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSPickUpStatusEffect : public AActor
{
	GENERATED_BODY()

public:
	ATDSPickUpStatusEffect();

protected:
	virtual void BeginPlay() override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	UPROPERTY(EditAnywhere)
	class USphereComponent* Sphere;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class UTDSStatusEffect> EffectClass;
};
