// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "TDSGameState.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API ATDSGameState : public AGameStateBase
{
	GENERATED_BODY()
	
	UPROPERTY(Replicated)
	TArray<class ATDSEnemy*> EnemyList;

	UPROPERTY(Replicated)
	int EnemyDefeated = 0;

	UPROPERTY(ReplicatedUsing = OnRep_CurrentWave)
	int CurrentWave = -1;
	UFUNCTION()
	void OnRep_CurrentWave();

	int WaveEnemySpawned = 0;
	int WaveEnemyDefeated = 0;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> StartWaveWidget;
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> EndWaveWidget;
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> EndGameWidget;

	UPROPERTY()
	UUserWidget* WaveDeclareWidget;

	UFUNCTION()
	void EnemyDeath(class ATDSEnemy* Enemy);

public:

	void AddEnemy(class ATDSEnemy* Enemy);
	void RemoveEnemy(class ATDSEnemy* Enemy);

	int GetDefeatedEnemyNum() const { return EnemyDefeated; }
	UFUNCTION(BlueprintCallable)
	int GetCurrentWave() const { return CurrentWave; }
	void SetCurrentWave(int NewWave) { CurrentWave = NewWave; }
	int GetWaveEnemySpawned() const { return WaveEnemySpawned; }
	int GetWaveEnemyDefeated() const { return WaveEnemyDefeated; }
	void IncWaveEnemySpawned() { WaveEnemySpawned++; }
	void IncWaveEnemyDefeated() { WaveEnemyDefeated++; }
	void ResetWaveEnemyCounters() { WaveEnemySpawned = WaveEnemyDefeated = 0; }

	void StartWave();
	
	UFUNCTION(NetMulticast, Reliable)
	void EndWave_NetMulticast();
	UFUNCTION(NetMulticast, Reliable)
	void EndGame_NetMulticast();

};
