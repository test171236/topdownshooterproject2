// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooterGameMode.h"

#include "UMG/Public/Blueprint/UserWidget.h"

#include "TopDownShooterPlayerController.h"
#include "GameFramework/PlayerStart.h"
#include "EngineUtils.h"
#include "../Character/TopDownShooterCharacter.h"
#include "../Environment/TDSEnemySpawner.h"
#include "../Enemies/TDSEnemy.h"
#include "TDSGameState.h"

ATopDownShooterGameMode::ATopDownShooterGameMode()
{
}

void ATopDownShooterGameMode::AddEnemySpawner(ATDSEnemySpawner* Spawner)
{
    EnemySpawnerList.AddUnique(Spawner);
}

void ATopDownShooterGameMode::EnemyDefeated(ATDSEnemy* Enemy)
{
    ATDSGameState* gs = GetGameState<ATDSGameState>();

    if (!gs || !WaveSettingsList.IsValidIndex(gs->GetCurrentWave()))
    {
        return;
    }

    gs->IncWaveEnemyDefeated();

    if (gs->GetWaveEnemyDefeated() >= WaveSettingsList[gs->GetCurrentWave()].EnemyNum)
    {
        EndWave();
    }
}

void ATopDownShooterGameMode::StartWave(int WaveNum)
{
    ATDSGameState* gs = GetGameState<ATDSGameState>();
    if (!gs || !WaveSettingsList.IsValidIndex(WaveNum))
    {
        return;
    }

    gs->SetCurrentWave(WaveNum);
    gs->ResetWaveEnemyCounters();

    gs->StartWave();

    if (WaveSettingsList[WaveNum].EnemyNum > 0)
    {
        GetWorld()->GetTimerManager().SetTimer(EnemySpawnTimer, this,
            &ATopDownShooterGameMode::SpawnEnemy, 0.1, true);
    }
    else
    {
        EndWave();
    }
}

void ATopDownShooterGameMode::EndWave()
{
    ATDSGameState* gs = GetGameState<ATDSGameState>();
    if (!gs)
    {
        return;
    }

    if (gs->GetCurrentWave() >= WaveSettingsList.Num() - 1)
    {
        gs->EndGame_NetMulticast();
    }
    else 
    {
        gs->EndWave_NetMulticast();
        GetWorld()->GetTimerManager().SetTimer(WaveStartTimer,
            [this, gs]() {StartWave(gs->GetCurrentWave() + 1); },
            TimeBetweenWaves,
            false);
    }
}

AActor* ATopDownShooterGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
    UWorld* World = GetWorld();
    for (TActorIterator<APlayerStart> It(World); It; ++It)
    {
        APlayerStart* Start = *It;
        if (Start && Start->PlayerStartTag == TEXT("Start"))
        {
            return Start;
        }
    }

    return Super::ChoosePlayerStart_Implementation(Player);
}

void ATopDownShooterGameMode::SpawnEnemy()
{
    ATDSGameState* gs = GetGameState<ATDSGameState>();
    
    if (!gs || gs->GetWaveEnemySpawned() >= WaveSettingsList[gs->GetCurrentWave()].EnemyNum)
    {
        GetWorld()->GetTimerManager().ClearTimer(EnemySpawnTimer);
        return;
    }

    int ind = FMath::RandRange(0, EnemySpawnerList.Num() - 1);
    float prob = FMath::FRand();
    const TArray<FWaveEnemySpawnSettings>& EnemySpawnSettingsList = 
        (WaveSettingsList[gs->GetCurrentWave()].EnemySpawnSettingsOverride.Num() > 0)
        ? WaveSettingsList[gs->GetCurrentWave()].EnemySpawnSettingsOverride
        : DefaultEnemySpawnSettings;
    TSubclassOf<class ATDSEnemy> EnemyClass = EnemySpawnSettingsList[0].EnemyClass;
    float total_prob = 0;

    for (int i = 0; i < EnemySpawnSettingsList.Num(); i++)
    {
        total_prob += EnemySpawnSettingsList[i].Prob;
        if (prob <= total_prob)
        {
            EnemyClass = EnemySpawnSettingsList[i].EnemyClass;
            break;
        }
    }

    if (EnemySpawnerList[ind]->SpawnEnemy(EnemyClass))
    {
        gs->IncWaveEnemySpawned();
    }
}

