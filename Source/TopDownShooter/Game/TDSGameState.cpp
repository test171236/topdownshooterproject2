// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameState.h"

#include "Net/UnrealNetwork.h"
#include "UMG/Public/Blueprint/UserWidget.h"

#include "../Enemies/TDSEnemy.h"
#include "TopDownShooterGameMode.h"

void ATDSGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATDSGameState, EnemyList);
    DOREPLIFETIME(ATDSGameState, CurrentWave);
}

void ATDSGameState::EnemyDeath(ATDSEnemy* Enemy)
{
    EnemyDefeated++;

    ATopDownShooterGameMode* gm = Cast<ATopDownShooterGameMode>(GetWorld()->GetAuthGameMode());
    if (gm)
    {
        gm->EnemyDefeated(Enemy);
    }
}

void ATDSGameState::AddEnemy(ATDSEnemy* Enemy)
{
    EnemyList.Add(Enemy);
    Enemy->OnEnemyDeath.AddDynamic(this, &ATDSGameState::EnemyDeath);
}

void ATDSGameState::RemoveEnemy(ATDSEnemy* Enemy)
{
    EnemyList.Remove(Enemy);
}

void ATDSGameState::OnRep_CurrentWave()
{
    StartWave();
}

void ATDSGameState::StartWave()
{
    if (WaveDeclareWidget)
    {
        WaveDeclareWidget->RemoveFromViewport();
    }
    WaveDeclareWidget = CreateWidget<UUserWidget>(GetWorld(), StartWaveWidget);
    if (WaveDeclareWidget)
    {
        WaveDeclareWidget->AddToViewport();
    }
}

void ATDSGameState::EndWave_NetMulticast_Implementation()
{
    if (WaveDeclareWidget)
    {
        WaveDeclareWidget->RemoveFromViewport();
    }
    WaveDeclareWidget = CreateWidget<UUserWidget>(GetWorld(), EndWaveWidget);
    if (WaveDeclareWidget)
    {
        WaveDeclareWidget->AddToViewport();
    }
}

void ATDSGameState::EndGame_NetMulticast_Implementation()
{
    CreateWidget<UUserWidget>(GetWorld(), EndGameWidget)->AddToViewport();
}
