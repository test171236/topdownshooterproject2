// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameInstance.h"

#include "../Character/Weapon/TDSWeapon.h"

bool UTDSGameInstance::GetWeaponInfoByName(FName WeaponName, FWeaponInfo& WeaponInfo)
{
    FWeaponInfo* info = WeaponInfoTable->FindRow<FWeaponInfo>(WeaponName, "");
    if (info)
    {
        WeaponInfo = *info;
        return true;
    }
    return false;
}

bool UTDSGameInstance::GetWeaponInfoByClass(TSubclassOf<ATDSWeapon> WeaponClass, FWeaponInfo& WeaponInfo)
{
    TArray<FWeaponInfo*> info_list;
    WeaponInfoTable->GetAllRows("", info_list);
    for (auto &info : info_list)
    {
        if (info->WeaponClass == WeaponClass)
        {
            WeaponInfo = *info;
            return true;
        }
    }
    return false;
}

