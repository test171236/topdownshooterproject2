// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "TDSPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API ATDSPlayerState : public APlayerState
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite)
	AActor* RespawnPoint;
	UPROPERTY(BlueprintReadWrite, ReplicatedUsing = OnRep_Color)
	FLinearColor Color = FLinearColor::White;
	UFUNCTION()
	void OnRep_Color();
};
