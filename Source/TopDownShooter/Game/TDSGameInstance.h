// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "../FuncLib/TDSTypes.h"
#include "TDSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	UDataTable* WeaponInfoTable = nullptr;

	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName WeaponName, FWeaponInfo& WeaponInfo);
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByClass(TSubclassOf<class ATDSWeapon> WeaponClass, FWeaponInfo& WeaponInfo);

};
