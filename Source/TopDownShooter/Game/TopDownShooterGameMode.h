// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TopDownShooterGameMode.generated.h"

USTRUCT()
struct FWaveEnemySpawnSettings {
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere)
	float Prob;
	UPROPERTY(EditAnywhere)
	TSubclassOf<class ATDSEnemy> EnemyClass;
};

USTRUCT()
struct FWaveSettings {
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere)
	int EnemyNum;
	UPROPERTY(EditAnywhere)
	TArray<FWaveEnemySpawnSettings> EnemySpawnSettingsOverride;
};

UCLASS(minimalapi)
class ATopDownShooterGameMode : public AGameModeBase
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<class ATDSEnemySpawner*> EnemySpawnerList;

	UPROPERTY(EditAnywhere)
	TArray<FWaveEnemySpawnSettings> DefaultEnemySpawnSettings;

	FTimerHandle EnemySpawnTimer;
	FTimerHandle WaveStartTimer;

	UPROPERTY(EditAnywhere)
	TArray<FWaveSettings> WaveSettingsList;
	UPROPERTY(EditAnywhere)
	float TimeBetweenWaves = 10;

	UFUNCTION()
	void SpawnEnemy();

public:
	ATopDownShooterGameMode();

	void AddEnemySpawner(class ATDSEnemySpawner* Spawner);
	void EnemyDefeated(class ATDSEnemy* Enemy);

	UFUNCTION(BlueprintCallable)
	void StartWave(int WaveNum);
	void EndWave();

	AActor* ChoosePlayerStart_Implementation(AController* Player) override;
};



