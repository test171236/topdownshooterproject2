// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TopDownShooterPlayerController.generated.h"

UCLASS()
class ATopDownShooterPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATopDownShooterPlayerController();

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnPossesedBy, ATopDownShooterCharacter*, PossessedCharacter, ATopDownShooterPlayerController*, NewController);
	UPROPERTY()
	FOnPossesedBy OnPossesedBy;

	UFUNCTION(BlueprintCallable)
	void Respawn();
protected:
	UPROPERTY()
	APawn* LastPawn;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	void OnPossess(APawn* InPawn) override;
};


