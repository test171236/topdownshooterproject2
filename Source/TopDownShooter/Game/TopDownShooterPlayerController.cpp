// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooterPlayerController.h"

#include "../Character/TopDownShooterCharacter.h"

ATopDownShooterPlayerController::ATopDownShooterPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ATopDownShooterPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}

void ATopDownShooterPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();
}

void ATopDownShooterPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	LastPawn = InPawn;

	ATopDownShooterCharacter* ch = Cast<ATopDownShooterCharacter>(InPawn);
	if (ch)
	{
		OnPossesedBy.Broadcast(ch, this);
	}
}

void ATopDownShooterPlayerController::Respawn()
{
	ATopDownShooterCharacter* ch = Cast<ATopDownShooterCharacter>(GetPawn());
	if (IsValid(ch) && IsLocalController())
	{
		//Possess(ch);
		ch->Respawn_Server();
	}
}
