// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSPlayerState.h"

#include "Net/UnrealNetwork.h"

#include "../Character/TopDownShooterCharacter.h"

void ATDSPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATDSPlayerState, Color);
}

void ATDSPlayerState::OnRep_Color()
{
    ATopDownShooterCharacter *ch = Cast<ATopDownShooterCharacter>(GetPawn());
    if (ch)
    {
        ch->SetSkinColor(Color);
    }
}
