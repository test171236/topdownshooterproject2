// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSStatusEffect_Burn.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Classes/Particles/ParticleSystemComponent.h"

void UTDSStatusEffect_Burn::InitEffect(AActor* Actor)
{
    Super::InitEffect(Actor);

    FireFXComponent = UGameplayStatics::SpawnEmitterAttached(FireFX,
        Actor->GetRootComponent(), TEXT(""), FVector::ZeroVector, FRotator(),
        EAttachLocation::KeepRelativeOffset);

    GetWorld()->GetTimerManager().SetTimer(EndTimer,
        this,
        &UTDSStatusEffect_Burn::DestroyEffect,
        Duration);
    GetWorld()->GetTimerManager().SetTimer(TickTimer,
        this,
        &UTDSStatusEffect_Burn::DamageTick,
        DamageTickInterval,
        true);
}

void UTDSStatusEffect_Burn::DestroyEffect()
{
    if (FireFXComponent)
    {
        FireFXComponent->Deactivate();
    }

    GetWorld()->GetTimerManager().ClearTimer(TickTimer);

    Super::DestroyEffect();
}

void UTDSStatusEffect_Burn::RefreshEffect()
{
    GetWorld()->GetTimerManager().SetTimer(EndTimer,
        this,
        &UTDSStatusEffect_Burn::DestroyEffect,
        Duration);
}

void UTDSStatusEffect_Burn::DamageTick()
{
    AffectedActor->TakeDamage(DamagePerTick, FPointDamageEvent(), nullptr, nullptr);
}

void UTDSStatusEffect_Burn::OnRep_AffectedActor(AActor* oldActor)
{
    Super::OnRep_AffectedActor(oldActor);

    if (!oldActor && AffectedActor)
    {
        FireFXComponent = UGameplayStatics::SpawnEmitterAttached(FireFX,
            AffectedActor->GetRootComponent(), TEXT(""), FVector::ZeroVector, FRotator(),
            EAttachLocation::KeepRelativeOffset);
    }
}
