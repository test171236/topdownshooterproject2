// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSStatusEffect.h"
#include "TDSStatusEffect_Regen.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TOPDOWNSHOOTER_API UTDSStatusEffect_Regen : public UTDSStatusEffect, public FTickableGameObject
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	float RegenRate = 10.f;
	UPROPERTY(EditAnywhere)
	float Duration = 5.f;
	
	FTimerHandle EndTimer;
	bool isTicking = false;

	UPROPERTY()
	class UTDSHealthSystem* HealthSystem;
	UPROPERTY(EditAnywhere)
	class UNiagaraSystem* HealingFX;
	UPROPERTY()
	class UNiagaraComponent* HealingFXComponent;

public:

	bool IsStackable() const override { return true; }
	void InitEffect(AActor* Actor) override;
	void DestroyEffect() override;

	virtual void Tick(float DeltaTime) override;

	virtual ETickableTickType GetTickableTickType() const override
	{
		return ETickableTickType::Conditional;
	}
	virtual TStatId GetStatId() const override
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(UTDSStatusEffect_Regen, STATGROUP_Tickables);
	}
	virtual bool IsTickableWhenPaused() const
	{
		return true;
	}
	virtual bool IsTickableInEditor() const
	{
		return false;
	}

	virtual bool IsTickable() const override { return isTicking; }

	void OnRep_AffectedActor(AActor* oldActor) override;

private:

	uint32 LastFrameNumberWeTicked = INDEX_NONE;
};
