// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSStatusEffect.h"

#include "Net/UnrealNetwork.h"

#include "TDSAffectedActor.h"

void UTDSStatusEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTDSStatusEffect, AffectedActor);
}

void UTDSStatusEffect::InitEffect(AActor* Actor)
{
    AffectedActor = Actor;

    ITDSAffectedActor* iact = Cast<ITDSAffectedActor>(AffectedActor);
    if (iact)
    {
        iact->GetEffectsList().Add(this);
    }
}

void UTDSStatusEffect::DestroyEffect()
{
    ITDSAffectedActor* iact = Cast<ITDSAffectedActor>(AffectedActor);
    if (iact)
    {
        iact->GetEffectsList().Remove(this);
    }

    AffectedActor = nullptr;

    ConditionalBeginDestroy();
}

UTDSStatusEffect* UTDSStatusEffect::CreateStatusEffect(AActor* AffectedActor, TSubclassOf<UTDSStatusEffect> EffectClass)
{
    if (!IsValid(EffectClass.GetDefaultObject()) || !AffectedActor->HasAuthority())
    {
        return nullptr;
    }

    ITDSAffectedActor* iact = Cast<ITDSAffectedActor>(AffectedActor);
    if (!iact || !iact->CanBeAffected())
    {
        return nullptr;
    }

    if (!EffectClass.GetDefaultObject()->IsStackable())
    {
        UTDSStatusEffect** effect_ptr = iact->GetEffectsList().FindByPredicate(
            [EffectClass](const UTDSStatusEffect* effect)->bool { return EffectClass == effect->GetClass(); });

        if (effect_ptr)
        {
            (*effect_ptr)->RefreshEffect();
            return *effect_ptr;
        }
    }

    UTDSStatusEffect* effect = NewObject<UTDSStatusEffect>(AffectedActor, EffectClass);
    effect->InitEffect(AffectedActor);

    return effect;
}

void UTDSStatusEffect::OnRep_AffectedActor(AActor* oldActor)
{
}
