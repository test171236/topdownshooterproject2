// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSStatusEffect.h"
#include "TDSStatusEffect_Burn.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TOPDOWNSHOOTER_API UTDSStatusEffect_Burn : public UTDSStatusEffect
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere)
	float Duration = 5.f;
	UPROPERTY(EditAnywhere)
	float DamagePerTick = 5.f;
	UPROPERTY(EditAnywhere)
	float DamageTickInterval = 1.f;
	UPROPERTY(EditAnywhere)
	class UParticleSystem* FireFX;
	UPROPERTY()
	class UParticleSystemComponent* FireFXComponent;

	FTimerHandle EndTimer;
	FTimerHandle TickTimer;

	UFUNCTION()
	void DamageTick();

public:
	bool IsStackable() const override { return false; }
	void InitEffect(AActor* Actor) override;
	void DestroyEffect() override;
	void RefreshEffect() override;
	void OnRep_AffectedActor(AActor* oldActor) override;
};
