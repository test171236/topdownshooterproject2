// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSStatusEffect_Regen.h"
#include "../Character/TDSHealthSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

void UTDSStatusEffect_Regen::InitEffect(AActor* Actor)
{
	Super::InitEffect(Actor);

    HealthSystem = Actor->FindComponentByClass<UTDSHealthSystem>();
    if (HealthSystem)
    {
        isTicking = true;
        HealingFXComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(HealingFX,
            Actor->GetRootComponent(), TEXT(""), FVector::ZeroVector, FRotator(),
            EAttachLocation::KeepRelativeOffset, true);
    }

    GetWorld()->GetTimerManager().SetTimer(EndTimer,
        this,
        &UTDSStatusEffect_Regen::DestroyEffect,
        Duration);

}

void UTDSStatusEffect_Regen::DestroyEffect()
{
    isTicking = false;
    if (HealingFXComponent)
    {
        HealingFXComponent->Deactivate();
    }

	Super::DestroyEffect();
}

void UTDSStatusEffect_Regen::Tick(float DeltaTime)
{
    if (LastFrameNumberWeTicked == GFrameCounter)
    {
        return;
    }

    if (HealthSystem)
    {
        HealthSystem->AddHP(RegenRate * DeltaTime);
    }

	LastFrameNumberWeTicked = GFrameCounter;
}

void UTDSStatusEffect_Regen::OnRep_AffectedActor(AActor* oldActor)
{
    Super::OnRep_AffectedActor(oldActor);

    if (!oldActor && AffectedActor)
    {
        HealthSystem = AffectedActor->FindComponentByClass<UTDSHealthSystem>();
        if (HealthSystem)
        {
            HealingFXComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(HealingFX,
                AffectedActor->GetRootComponent(), TEXT(""), FVector::ZeroVector, FRotator(),
                EAttachLocation::KeepRelativeOffset, true);
        }
    }
}
