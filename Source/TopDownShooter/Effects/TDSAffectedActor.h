// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDSAffectedActor.generated.h"

UINTERFACE(MinimalAPI)
class UTDSAffectedActor : public UInterface
{
	GENERATED_BODY()
};

class TOPDOWNSHOOTER_API ITDSAffectedActor
{
    GENERATED_BODY()

public:

    virtual TArray<class UTDSStatusEffect*>& GetEffectsList() = 0;
    virtual bool CanBeAffected() const { return true; }
    virtual void BeginStun() {}
    virtual void EndStun() {}

    UFUNCTION()
    virtual void OnRep_EffectsList(const TArray<class UTDSStatusEffect*>& oldEffectsList);
};

/*USTRUCT()
struct TOPDOWNSHOOTER_API FTDSStatusEffectParametrs
{
    GENERATED_BODY()
public:
     UPROPERTY()
     TArray<class UTDSStatusEffect*> EffectsList;

}; */