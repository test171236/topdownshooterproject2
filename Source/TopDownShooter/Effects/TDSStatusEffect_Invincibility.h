// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSStatusEffect.h"
#include "TDSStatusEffect_Invincibility.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TOPDOWNSHOOTER_API UTDSStatusEffect_Invincibility : public UTDSStatusEffect
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	float Duration = 5.f;
	UPROPERTY()
	class UTDSHealthSystem* HealthSystem;
	UPROPERTY(EditAnywhere)
	class UNiagaraSystem* InvincibilityFX;
	UPROPERTY()
	class UNiagaraComponent* InvincibilityFXComponent;

	FTimerHandle EndTimer;

public:
	bool IsStackable() const override { return false; }
	void InitEffect(AActor* Actor) override;
	void DestroyEffect() override;
	void RefreshEffect() override;

	void OnRep_AffectedActor(AActor* oldActor) override;
};
