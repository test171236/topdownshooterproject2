// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSStatusEffect.h"
#include "TDSStatusEffect_Stun.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TOPDOWNSHOOTER_API UTDSStatusEffect_Stun : public UTDSStatusEffect
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere)
	float Duration = 5.f;

	FTimerHandle EndTimer;

public:
	bool IsStackable() const override { return false; }
	void InitEffect(AActor* Actor) override;
	void DestroyEffect() override;
	void RefreshEffect() override;
	void SetStunDuration(float Dur);

	void OnRep_AffectedActor(AActor* oldActor) override;
};
