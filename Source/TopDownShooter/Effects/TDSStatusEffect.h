// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TDSStatusEffect.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSStatusEffect : public UObject
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(ReplicatedUsing = OnRep_AffectedActor)
	AActor* AffectedActor;

public:
	virtual bool IsSupportedForNetworking() const override { return true; }

	virtual bool IsStackable() const { return true; }

	virtual void InitEffect(AActor* Actor);
	virtual void DestroyEffect();
	virtual void RefreshEffect() {}

	static UTDSStatusEffect* CreateStatusEffect(AActor* AffectedActor, TSubclassOf<UTDSStatusEffect> EffectClass);

	UFUNCTION()
	virtual void OnRep_AffectedActor(AActor* oldActor);
};
