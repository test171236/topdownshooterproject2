// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSStatusEffect_Invincibility.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

#include "../Character/TDSHealthSystem.h"

void UTDSStatusEffect_Invincibility::InitEffect(AActor* Actor)
{
    Super::InitEffect(Actor);

    HealthSystem = Actor->FindComponentByClass<UTDSHealthSystem>();
    if (HealthSystem)
    {
        HealthSystem->SetIsInvincible(true);
        InvincibilityFXComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(InvincibilityFX,
            Actor->GetRootComponent(), FName(), FVector::ZeroVector, FRotator::ZeroRotator,
            EAttachLocation::SnapToTarget, true);
    }

    GetWorld()->GetTimerManager().SetTimer(EndTimer,
        this,
        &UTDSStatusEffect_Invincibility::DestroyEffect,
        Duration);
}

void UTDSStatusEffect_Invincibility::DestroyEffect()
{
    if (HealthSystem)
    {
        HealthSystem->SetIsInvincible(false);
    }
    if (InvincibilityFXComponent)
    {
        InvincibilityFXComponent->Deactivate();
    }

    Super::DestroyEffect();
}

void UTDSStatusEffect_Invincibility::RefreshEffect()
{
    GetWorld()->GetTimerManager().SetTimer(EndTimer,
        this,
        &UTDSStatusEffect_Invincibility::DestroyEffect,
        Duration);
}

void UTDSStatusEffect_Invincibility::OnRep_AffectedActor(AActor* oldActor)
{
    Super::OnRep_AffectedActor(oldActor);

    if (!oldActor && AffectedActor)
    {
        HealthSystem = AffectedActor->FindComponentByClass<UTDSHealthSystem>();
        if (HealthSystem)
        {
            HealthSystem->SetIsInvincible(true);
            InvincibilityFXComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(InvincibilityFX,
                AffectedActor->GetRootComponent(), FName(), FVector::ZeroVector, FRotator::ZeroRotator,
                EAttachLocation::SnapToTarget, true);
        }
    }
}
