// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSStatusEffect_Stun.h"
#include "TDSAffectedActor.h"

void UTDSStatusEffect_Stun::InitEffect(AActor* Actor)
{
    Super::InitEffect(Actor);

    ITDSAffectedActor* iact = Cast<ITDSAffectedActor>(AffectedActor);
    if (iact)
    {
        iact->BeginStun();
    }

    GetWorld()->GetTimerManager().SetTimer(EndTimer, 
        this,
        &UTDSStatusEffect_Stun::DestroyEffect,
        Duration);
}

void UTDSStatusEffect_Stun::DestroyEffect()
{
    ITDSAffectedActor* iact = Cast<ITDSAffectedActor>(AffectedActor);
    if (iact)
    {
        iact->EndStun();
    }

    Super::DestroyEffect();
}

void UTDSStatusEffect_Stun::RefreshEffect()
{
    GetWorld()->GetTimerManager().SetTimer(EndTimer,
        this,
        &UTDSStatusEffect_Stun::DestroyEffect,
        Duration);
}

void UTDSStatusEffect_Stun::SetStunDuration(float Dur)
{
    Duration = Dur;
    RefreshEffect();
}

void UTDSStatusEffect_Stun::OnRep_AffectedActor(AActor* oldActor)
{
    Super::OnRep_AffectedActor(oldActor);

    if (!oldActor && AffectedActor)
    {
        ITDSAffectedActor* iact = Cast<ITDSAffectedActor>(AffectedActor);
        if (iact)
        {
            iact->BeginStun();
        }
    }
}
