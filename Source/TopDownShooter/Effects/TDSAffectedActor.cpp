// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSAffectedActor.h"

#include "TDSStatusEffect.h"

void ITDSAffectedActor::OnRep_EffectsList(const TArray<class UTDSStatusEffect*>& oldEffectsList)
{
	for (auto old_effect : oldEffectsList)
	{
		if (old_effect && !GetEffectsList().Contains(old_effect))
		{
			old_effect->DestroyEffect();
		}
	}
}
